import pandas as pd
import numpy as np
import json 
import requests
import time
from DeribitPull import get_vols, get_vol_surface
from deribit_api import RestClient
import sentry_sdk
from sentry_sdk import capture_exception, capture_message


EXCEPTION_DELAY = 60

def gen_vol_dict(vol_surface, spot_price, strikes, expiries, epoch=True, vol_bump=-0.1):
	vol_dict = {}
	for strike in strikes:
		vol_dict[str(strike)] = {}
		for expiry in expiries:
			if ~epoch and (isinstance(expiry, float) or isinstance(expiry, int)):
				vol_dict[str(strike)][str(expiry)] = float(vol_surface(expiry / 52, spot_price * (1 + strike))) / 100 + vol_bump
			elif isinstance(expiry, pd.Timestamp):
				epoch_expiry = timestamp_to_epoch(expiry)
				expiry_days = (expiry - pd.Timestamp.now().normalize()).days
				vol_dict[str(strike)][str(epoch_expiry)] = float(vol_surface(expiry_days / 365, spot_price * (1 + strike))) / 100 + vol_bump
	return vol_dict

def post_expiry(token, expiry, epoch=True):
	if epoch:
		epoch_expiry = [timestamp_to_epoch(x) for x in expiry]
		sample_expiry = {
		    "expiry": epoch_expiry
		}
	else:
		sample_expiry = {
		    "expiry": expiry
		}
	head = {'x-access-token': token, 'Content-type': 'application/json'}
	url = divibit_url + '/v1/config/expiry-terms'
	r = requests.post(url, headers=head, data=json.dumps(sample_expiry))

def post_strike(token, strikes):
	sample_strikes = {
    "strike": strikes
	}
	head = {'x-access-token': token, 'Content-type': 'application/json'}
	url = divibit_url + '/v1/config/strike-prices'
	r = requests.post(url, headers=head, data=json.dumps(sample_strikes))

def post_rate(token, ir):
	sample_rate = {
    	"rate": ir
	}
	head = {'x-access-token': token, 'Content-type': 'application/json'}
	url = divibit_url + '/v1/config/rate'
	r = requests.post(url, headers=head, data=json.dumps(sample_rate))


def post_vols(token, vols, coin='BTC'):
	sample_vols = {
	    "volatility": vols
	}
	head = {'x-access-token': token, 'Content-type': 'application/json'}
	url = divibit_url + '/v1/config/volatility/' + coin
	r = requests.post(url, headers=head, data=json.dumps(sample_vols))

def timestamp_to_epoch(timestamp):
	return int((timestamp - pd.Timestamp('1970-01-01')).total_seconds())

KEY = '5RQV61aMc3sra'
SECRET = 'D2JRQ22JLSK3RBXUXCCG33ACZFKR2MKB'
URL = 'https://www.deribit.com'


divibit_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI5NDQ4YmE5ZS1mMmE0LTRlODMtODdjMS1jYTk0YjE0YzA4OGUiLCJwayI6IiIsImFkbWluIjp0cnVlLCJpYXQiOjE1NTcwOTcwNzMsImV4cCI6MTU2MjI4MTA3MywiaXNzIjoiYXJjYW5hLmlvIn0.zYBMwjlMc89KjGySE2bW_lYwTKU9gmvMZh8DxYEYwwk'
divibit_url = 'https://dev-admin-api.divibit.com'


def main():
	strike_list = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35]
	client = RestClient(KEY, SECRET, url=URL)
	sentry_sdk.init("https://2e7756d69f1a4c6d9815ebd1ce4dadae@sentry.io/1459753")

	sample_expiry = {'username': 'admin@payton', 'password': 'divibit-admin-123'}

	head = {'Content-type': 'application/json'}
	url = 'https://dev-admin-api.divibit.com/v1/login'
	r = requests.post(url, headers=head, data=json.dumps(sample_expiry))
	token = r.json()['token']

	while True:
		try:

			surface_btc, spot_btc, rate_btc, call_options_btc, futures_btc = get_vol_surface(client, vol_type='bid', coin='BTC')
			surface_eth, spot_eth, rate_eth, call_options_eth, futures_eth = get_vol_surface(client, vol_type='bid', coin='ETH')

			call_options_btc.to_pickle('btc_call_options.p')
			expiries = pd.DatetimeIndex(call_options_btc['expiration']).tz_localize(None).unique()
			expiry_list_btc = [x for x in expiries if (((x - pd.Timestamp.now().normalize()).days >= 3) and ((x - pd.Timestamp.now().normalize()).days <= 160))]
			post_rate(divibit_token, rate_btc)
			print('Posted BTC Rate {} at {}'.format(rate_btc, pd.Timestamp.now()))
			vol_json = gen_vol_dict(surface_btc, spot_btc, strike_list, expiry_list_btc, epoch=True, vol_bump=-0.1)
			print('BTC Expiries: {}'.format(expiry_list_btc))
			post_strike(token, strike_list)
			post_expiry(token, expiry_list_btc, epoch=True)
			post_vols(token, vol_json, coin='BTC')

			print('Posted BTC Vols at {}'.format(pd.Timestamp.now()))
			print(vol_json)

			expiries = pd.DatetimeIndex(call_options_eth['expiration']).tz_localize(None).unique()
			expiry_list_eth = [x for x in expiries if (((x - pd.Timestamp.now().normalize()).days >= 14) and ((x - pd.Timestamp.now().normalize()).days <= 160))]
			vol_json = gen_vol_dict(surface_eth, spot_eth, strike_list, expiry_list_eth, epoch=True, vol_bump=-0.1)
			print('ETH Expiries: {}'.format(expiry_list_eth))
			post_strike(token, strike_list)
			post_expiry(token, expiry_list_eth, epoch=True)
			post_vols(divibit_token, vol_json, coin='ETH')

			print('Posted ETH Vols at {}'.format(pd.Timestamp.now()))
			print(vol_json)

			time.sleep(120)
		except KeyboardInterrupt:
			print('Manual break by user')
			capture_message('Divibit Vol Upload Manual Shutdown')
			return
		except Exception as e:
			capture_message('Divibit Vol Upload Exception Caught. Attempting to Restart: \n {}'.format(e))
			capture_exception(e)
			time.sleep(EXCEPTION_DELAY)
			client = RestClient(KEY, SECRET, url=URL)
			head = {'Content-type': 'application/json'}
			url = 'https://dev-admin-api.divibit.com/v1/login'
			r = requests.post(url, headers=head, data=json.dumps(sample_expiry))
			token = r.json()['token']


if __name__=='__main__':
	main()
