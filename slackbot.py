import os
import time
import re
from slackclient import SlackClient
import pandas as pd
import sentry_sdk
from pathlib import Path
import pickle
from deribit_api import RestClient
import requests
import json 
from requests.exceptions import ConnectionError
from sentry_sdk import capture_exception, capture_message



#deribit client
client = RestClient('Js9aG3pPLaQb', 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO', url='https://test.deribit.com')
#client = RestClient('5RQV61aMc3sra', 'D2JRQ22JLSK3RBXUXCCG33ACZFKR2MKB', url='https://www.deribit.com')


# instantiate Slack client
slack_client = SlackClient('xoxb-624488884355-638108144246-rB6KPObB76cV70lBBKZzPUXS')
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
RTM_READ_DELAY = 1 # 1 second delay between reading from RTM
OUTPUT_DELAY = 60*60 # 1hour every slackbot auto message
CHECK_NEW_TRADES = 60 #Check for new Divibit Trades every minute
CHECK_DERIBIT_MARGIN = 60  # Check Deribit Margin every minute
EXCEPTION_DELAY = 60 # Pause for 60seconds if there's an exception and retry

EXAMPLE_COMMAND = "BTC Risk"
ETH_COMMAND = 'ETH Risk'
BTC_DELTA_COMMAND = 'btc delta'
ETH_DELTA_COMMAND = 'eth delta'
BTC_GAMMA_COMMAND = 'btc gamma'
ETH_GAMMA_COMMAND = 'eth gamma'
BTC_VEGA_COMMAND = 'btc vega'
ETH_VEGA_COMMAND = 'eth vega'
MARGIN_COMMAND = 'deribit margin'
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == starterbot_id:
                return message, event["channel"]
    return None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    default_response = "Not sure what you mean. Try *{}*.".format(EXAMPLE_COMMAND)

    # Finds and executes the given command, filling in response
    response = None
    # This is where you start to implement more commands!
    btc_risk = pd.read_pickle('all_btc_greeks.p')
    eth_risk = pd.read_pickle('all_eth_greeks.p')
    if command.startswith(EXAMPLE_COMMAND):
        response = btc_risk.to_string()
        #response = "Sure...write some more code then I can do that!"
    elif command.startswith(ETH_COMMAND):
        
        response = eth_risk.to_string()
    elif command.startswith(ETH_DELTA_COMMAND):
        response = 'Delta: ${}'.format(eth_risk.sum()['Delta'])
    elif command.startswith(BTC_DELTA_COMMAND):
        response = 'Delta: ${}'.format(btc_risk.sum()['Delta'])
    elif command.startswith(ETH_GAMMA_COMMAND):
        response = 'Gamma: ${}'.format(eth_risk.sum()['Gamma'])
    elif command.startswith(BTC_GAMMA_COMMAND):
        response = 'Gamma: ${}'.format(btc_risk.sum()['Gamma'])
    elif command.startswith(ETH_VEGA_COMMAND):
        response = 'Vega: ${}'.format(eth_risk.sum()['Vega'])
    elif command.startswith(BTC_VEGA_COMMAND):
        response = 'Vega: ${}'.format(btc_risk.sum()['Vega'])
    elif command.startswith(MARGIN_COMMAND):
        deribit_details = client.account()
        available_funds = deribit_details['availableFunds']
        balance = deribit_details['balance']
        pct_available = available_funds / balance
    
        response = '{}% Balance Available \n'.format(round(pct_available * 100))
        response += pd.Series(deribit_details).to_string()

    # Sends the response back to the channel
    if response is None:
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=default_response
        )
    else:
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=response,
        ) 

def send_greeks_update(channel):
    btc_risk = pd.read_pickle('all_btc_greeks.p')
    eth_risk = pd.read_pickle('all_eth_greeks.p')
    for coin in ['btc', 'eth']:
        risk = pd.read_pickle('all_{}_greeks.p'.format(coin))
        response = []
        response.append('{} delta: ${}'.format(coin, risk.sum()['Delta']))
        response.append('{} gamma: ${}'.format(coin, risk.sum()['Gamma']))
        response.append('{} vega: ${}'.format(coin, risk.sum()['Vega']))
        for r in response:
            slack_client.api_call(
                    "chat.postMessage",
                    channel=channel,
                    text=r
            )
            
def check_new_trades(channel):
    sample_expiry = {
    'username': 'admin@payton',
    'password': 'divibit-admin-123'
    }
    head = {'Content-type': 'application/json'}
    url = 'https://dev-admin-api.divibit.com/v1/login'
    r = requests.post(url, headers=head, data=json.dumps(sample_expiry))
    token = r.json()['token']

    url = 'https://dev-admin-api.divibit.com/v1/trades'
    head = {'x-access-token': token}
    res = requests.get(url, headers=head)
    positions = pd.DataFrame(res.json())
    positions.index = positions['id']
    positions['t'] = (pd.DatetimeIndex(positions['expiryTs']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365

    processed_file = Path('slack_processed.p')
    prev_processed_trades = None
    if processed_file.is_file():
        prev_processed_trades = pickle.load( open( 'slack_processed.p', 'rb' ) )
    if prev_processed_trades is not None:
        need_to_process = [tradeId for tradeId in positions.index if (tradeId not in prev_processed_trades.index) and (positions.loc[tradeId]['t'] > 0)]
    else:
        need_to_process = [tradeId for tradeId in positions.index if (positions.loc[tradeId]['t'] > 0)]
    
    for new_id in need_to_process:
        position = positions.loc[new_id]
        response = 'New Trade: {} {} {} {} ({}%) @ {}%'.format(position['amount'], 
                                                               position['assetId'],
                                                               position['expiryTs'], 
                                                               position['strikePrice'],
                                                               float(position['strikePercentage']) * 100,
                                                               position['yieldPct'])
        slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=response
        )
    positions.to_pickle('slack_processed.p')

# TODO Account only gets BTC margin, need to find out how to get ETH. Need to write the v2 function. https://docs.deribit.com/v2/?python#private-get_account_summary   
def check_deribit_margin(channel):
    deribit_details = client.account()
    available_funds = deribit_details['availableFunds']
    balance = deribit_details['balance']
    pct_available = available_funds / balance
    
    if pct_available < 0.25:
        response = 'WARNING, > 75% OF FUNDS ARE USED FOR MARGIN. {}% FREE'.format(round(pct_available * 100))
        slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=response
        )
        slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=deribit_details
        )
        
        
if __name__ == "__main__":
    sentry_sdk.init("https://2e7756d69f1a4c6d9815ebd1ce4dadae@sentry.io/1459753")
    
    if slack_client.rtm_connect(with_team_state=False):
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        
        counter = 0
        new_trades_counter = 0
        margin_counter = 0
        while True:
            try:
                command, channel = parse_bot_commands(slack_client.rtm_read())
                if command:
                    handle_command(command, channel)
                
                counter += 1
                if counter == OUTPUT_DELAY:
                    counter = 0
                    send_greeks_update('bot_outputs')
                
                new_trades_counter += 1
                if new_trades_counter == CHECK_NEW_TRADES:
                    new_trades_counter = 0
                    check_new_trades('new_divibit_trades')
                    
                margin_counter += 1
                if margin_counter == CHECK_DERIBIT_MARGIN:
                    margin_counter = 0
                    check_deribit_margin('deribit_margin')
                    
                # Check and report on Deribit Margin
                time.sleep(RTM_READ_DELAY)
            except Exception as e:
                capture_message('Slackbot Exception Caught: \n {}'.format(e))
                capture_exception(e)
                time.sleep(EXCEPTION_DELAY) 
                print(e)
                slack_client = SlackClient('xoxb-624488884355-638108144246-rB6KPObB76cV70lBBKZzPUXS')

                while ~slack_client.rtm_connect(with_team_state=False):
                    slack_client = SlackClient('xoxb-624488884355-638108144246-rB6KPObB76cV70lBBKZzPUXS')
                    client = RestClient('Js9aG3pPLaQb', 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO', url='https://test.deribit.com')
                starterbot_id = slack_client.api_call("auth.test")["user_id"]
                print('Reconnected to Chatbot')


    else:
        print("Connection failed. Exception traceback printed above.")