from OptionPricer import get_iv, BlackScholes
import numpy as np
import pandas as pd
import pickle
import json 
import requests
from deribit_api import RestClient
from pathlib import Path
from scipy.stats import norm
from itertools import groupby
from DeribitPull import get_vol_surface
import operator
import time
import sentry_sdk
from sentry_sdk import capture_exception, capture_message


EXCEPTION_DELAY = 60

def sell_option(call_options, expiry, strike, size, rounding=0.1):
    #sell closest strike call. 
    call_options['normalized expiry'] = pd.DatetimeIndex(call_options['expiration']).tz_localize(None).normalize()
    same_expiry_options = call_options[call_options['normalized expiry'] == expiry]
    if strike in list(same_expiry_options['strike']):
        return [same_expiry_options[same_expiry_options['strike'] == strike]['instrumentName'][0]], [size]
    
    upper_option = None
    lower_option = None
    if strike < same_expiry_options['strike'].max():
        upper_option = abs(same_expiry_options[same_expiry_options['strike'] > strike]['strike'] - strike).argmin()
    if strike > same_expiry_options['strike'].min():
        lower_option = abs(same_expiry_options[same_expiry_options['strike'] < strike]['strike'] - strike).argmin()    
    
    if upper_option is None:
        return [lower_option], [size]
    if lower_option is None:
        return [upper_option], [size]
    
    upper_option_dist = same_expiry_options.loc[upper_option]['strike'] - strike
    lower_option_dist = strike - same_expiry_options.loc[lower_option]['strike']
    lower_option_size = size * lower_option_dist / (upper_option_dist + lower_option_dist)
    lower_option_size = round(lower_option_size / rounding) * rounding
    upper_option_size = size - lower_option_size
    return [lower_option, upper_option], [lower_option_size, upper_option_size]


    
    
def get_position_greeks(client, call_options):
    positions = pd.DataFrame(client.positions())
    positions.index = positions['instrument']
    positions = pd.concat([positions, call_options.loc[positions.index]], axis=1)
    positions['notional'] = positions['spot price'] * positions['amount']
    
    position_greeks = pd.DataFrame()
    for pos in positions.index:
        position = positions.loc[pos]
        bs = BlackScholes(position['spot price'], position['strike'], position['rate'], 0, position['IV mid'], position['t'], 'call')
        position_greeks = position_greeks.append(pd.DataFrame({'Delta': bs.delta() * position['spot price'] * position['amount'],
                                                               'Gamma': bs.gamma() / 100 * position['spot price']**2 * position['amount'],
                                                               'Vega': bs.vega() / 100 * position['amount'],
                                                               'Theta': bs.theta() * position['amount'],
                                                               'Size': position['amount'],
                                                               'Notional': position['spot price'] * position['amount'],
                                                               '% Strike': position['strike'] / position['spot price'] * 100}, index=[pos]))
    position_greeks = position_greeks.append(pd.DataFrame(position_greeks.sum().to_dict(), index=['Total']))
    return position_greeks




    
def get_mid_price(client, instrument_name, pricing_method='regular'):
    orderbook = client.getorderbook(instrument_name)
    bids = pd.DataFrame(orderbook['bids'])
    asks = pd.DataFrame(orderbook['asks'])
    if len(bids) == 0 or len(asks) == 0:
        return np.nan
    return (bids.iloc[0]['price'] + asks.iloc[0]['price']) / 2


def get_bid_price(client, instrument_name, pricing_method='regular'):
    orderbook = client.getorderbook(instrument_name)
    bids = pd.DataFrame(orderbook['bids'])
    if len(bids) == 0:
        return np.nan
    return bids.iloc[0]['price']

def insert_iv_to_df(call_options, rate, spot_price, price_type='bid'):
    today = pd.Timestamp.now().normalize()
    call_iv = {}
    for call in call_options.index:
        call_option = call_options.loc[call]
        if (price_type is 'mid') and (~np.isnan(call_option['mid'])):
            call_iv[call] = get_iv(call_option['mid'] * 100, 'call', 100, call_option['strike'] / spot_price * 100, rate, 0, call_option['t'])
        elif (price_type is 'bid') and (~np.isnan(call_option['bid'])):
            call_iv[call] = get_iv(call_option['bid'] * 100, 'call', 100, call_option['strike'] / spot_price * 100, rate, 0, call_option['t'])
        else:
            call_iv[call] = np.nan
    call_options['IV '+price_type] = pd.Series(call_iv)
    return call_options

def get_single_rate(futures, coin='BTC'):
    swap_price = futures.loc[coin + '-PERPETUAL']['mid']
    rates = []
    for future in futures.index:
        if future != coin + '-PERPETUAL':
            current_future = futures.loc[future]
            expiry_t = (pd.Timestamp(current_future['expiration']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365
            current_future_rate = ((current_future['mid'] / swap_price) - 1) / expiry_t
            rates.append(current_future_rate)
    return np.mean(rates)


def get_all_instruments(client, coin='BTC'):
    instruments = pd.DataFrame(client.getinstruments())
    futures = instruments[(instruments['kind'] == 'future') & (instruments['baseCurrency'] == coin)]
    futures.index = futures['instrumentName']
    future_prices = {}
    for future in futures.index:
        future_prices[future] = get_mid_price(client, future)

    futures['mid'] = pd.Series(future_prices)
    spot_price = futures.loc[coin + '-PERPETUAL']['mid']
    rate = get_single_rate(futures, coin=coin)

    call_options = instruments[(instruments['kind'] == 'option') & (instruments['baseCurrency'] == coin) & (instruments['optionType'] == 'call')]
    call_options.index = call_options['instrumentName']


    call_prices_mid = {}
    call_prices_bid = {}
    for call in call_options.index:
        call_prices_mid[call] = get_mid_price(client, call)
        call_prices_bid[call] = get_bid_price(client, call)

    call_options['mid'] = pd.Series(call_prices_mid)
    call_options['bid'] = pd.Series(call_prices_bid)
    call_options['t'] = (pd.DatetimeIndex(call_options['expiration']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365
    call_options = insert_iv_to_df(call_options, rate, spot_price, price_type='bid')
    call_options = insert_iv_to_df(call_options, rate, spot_price, price_type='mid')
    call_options['spot price'] = spot_price
    call_options['rate'] = rate


def get_deribit_greeks(client, spot_price, irate, vol_surface, call_options, futures, coin='BTC'):
    positions = pd.DataFrame(client.positions())
    if len(positions) == 0:
        return pd.DataFrame()
    positions.index = positions['instrument']
    positions = positions[positions['instrument'].str.contains(coin)]
    futures_overlap = [value for value in positions.index if value in futures.index]
    if len(futures_overlap) > 0:
        positions = positions.combine_first(futures.loc[positions.index])
    calls_overlap = [value for value in positions.index if value in call_options.index]    
    if len(calls_overlap) > 0:
        positions = positions.combine_first(call_options.loc[positions.index])
    #TODO check excpetion if the option is no longer traded (is this possible?)
    positions = positions[positions['instrument'].str.contains(coin)]
    
    positions['notional'] = spot_price * positions['amount']
    
    position_greeks = pd.DataFrame()
    for pos in positions.index:
        position = positions.loc[pos]
        gamma = 0
        vega = 0
        theta = 0
        pct_strike = np.nan
        strike = np.nan
        iv = np.nan
        expiry = np.nan
        dte = np.nan
        if position['kind'] == 'option':
            expiry = pd.Timestamp(position['expiration']).tz_localize(None)
            dte = (expiry - pd.Timestamp.now().normalize()).days
            pct_strike = position['strike'] / spot_price * 100
            strike = position['strike']
            notional = spot_price * position['amount']
            if np.isnan(position['IV']):
                iv = float(vol_surface(position['t'], strike))
            else:
                iv = position['IV']
            bs = BlackScholes(spot_price, position['strike'], irate, 0, iv, position['t'], 'call')
            delta = bs.delta() * spot_price * position['amount']
            gamma = bs.gamma() / 100 * spot_price**2 * position['amount']
            vega = bs.vega() / 100 * position['amount']
            theta = bs.theta() * position['amount']

        else:
            delta = position['amount']
            notional = position['amount']
        
        position_greeks = position_greeks.append(pd.DataFrame({'Delta': delta,
                                                               'Gamma': gamma,
                                                               'Vega': vega,
                                                               'Theta': theta,
                                                               'Size': position['amount'],
                                                               'Notional': notional,
                                                               '% Strike': pct_strike,
                                                               'Strike': strike,
                                                               'Spot': spot_price,
                                                               'Expiry': expiry,
                                                               'Days to Expiry': dte,
                                                               'IV': iv,
                                                               }, index=[pos]))
    return position_greeks

def get_divibit_greeks(token, vol_surface, spot_price, irate, coin='BTC'):
    url = 'https://dev-admin-api.divibit.com/v1/trades'
    head = {'x-access-token': token}
    res = requests.get(url, headers=head)
    positions = pd.DataFrame(res.json())
    positions = positions[positions['assetId'] == coin]
    positions.index = positions['id']
    positions['amount'] = positions['amount'].convert_objects(convert_numeric=True)
    positions['notional'] = spot_price * positions['amount']
    positions['strikePrice'] = positions['strikePrice'].convert_objects(convert_numeric=True)
    positions['t'] = (pd.DatetimeIndex(positions['expiryTs']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365
    
    position_greeks = pd.DataFrame()
    for pos in positions.index:
        position = positions.loc[pos]
        if position['t'] > 0:
            iv = float(vol_surface(position['t'], position['strikePrice']))
            bs = BlackScholes(spot_price, position['strikePrice'], irate, 0, iv, position['t'], 'call')
            expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
            position_greeks = position_greeks.append(pd.DataFrame({'Delta': bs.delta() * spot_price * position['amount'],
                                                                   'Gamma': bs.gamma() / 100 * spot_price**2 * position['amount'],
                                                                   'Vega': bs.vega() / 100 * position['amount'],
                                                                   'Theta': bs.theta() * position['amount'],
                                                                   'Size': position['amount'],
                                                                   'Notional': spot_price * position['amount'],
                                                                   '% Strike': position['strikePrice'] / spot_price * 100,
                                                                   'Strike': position['strikePrice'],
                                                                   'Spot': spot_price,
                                                                   'Expiry': expiry,
                                                                   'Days to Expiry': (expiry - pd.Timestamp.now().normalize()).days,
                                                                   'IV': iv,
                                                                   'TradeID': position['id']}, index=[pos]))
    return position_greeks


def process_new_divibit_trades(token, client, call_options, surfaces, spot_prices, rates, aggressive=-10):
    url = 'https://dev-admin-api.divibit.com/v1/trades'
    head = {'x-access-token': token}
    res = requests.get(url, headers=head)
    positions = pd.DataFrame(res.json())
    positions.index = positions['id']
    positions['amount'] = positions['amount'].convert_objects(convert_numeric=True)
    positions['strikePrice'] = positions['strikePrice'].convert_objects(convert_numeric=True)
    positions['t'] = (pd.DatetimeIndex(positions['expiryTs']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365
    processed_file = Path('processed.p')
    prev_processed_trades = None
    if processed_file.is_file():
        prev_processed_trades = pickle.load( open( 'processed.p', 'rb' ) )
    if prev_processed_trades is not None:
        need_to_process = [tradeId for tradeId in positions.index if (tradeId not in prev_processed_trades.index) and (positions.loc[tradeId]['t'] > 0)]
    else:
        need_to_process = [tradeId for tradeId in positions.index if (positions.loc[tradeId]['t'] > 0)]
    print(need_to_process)
    #Iterate through the trades to process and call hedge trade on each one to generate the trade order
    hedge_trade_list = pd.DataFrame()
    for pos in need_to_process:
        position = positions.loc[pos]
        if position['assetId'] == 'BTC':
            undl = 'BTC'
            hedge_order = None
            if position['amount'] >= 0.1:
                hedge_order = hedge_trade(client, call_options['BTC'], position, aggressive=aggressive)
                if isinstance(hedge_order, pd.DataFrame):
                    hedge_trade_list = hedge_trade_list.append(hedge_order)
            
            if (position['amount'] < 0.1) or (hedge_order is None):
#             if (position['amount'] < 0.1) or (hedge_order == np.nan):
                expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
                hedge_size = delta_hedge(surfaces['BTC'], position['amount'], position['strikePrice'], expiry, spot_prices['BTC'], rates['BTC'])
                hedge_trade_list = hedge_trade_list.append(pd.DataFrame({'Instrument': undl+'-PERPETUAL', 'Size': hedge_size}, index=['Trade']))
                # send just delta hedge
        elif position['assetId'] == 'ETH':
            undl = 'ETH'
            if position['amount'] >= 1:
                hedge_order = hedge_trade(client, call_options['ETH'], position, aggressive=aggressive)
                if isinstance(hedge_order, pd.DataFrame):
                    hedge_trade_list = hedge_trade_list.append(hedge_order)
            if (position['amount'] < 1) or (hedge_order is None):
                # send just delta hedge
                expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
                hedge_size = delta_hedge(surfaces['ETH'], position['amount'], position['strikePrice'], expiry, spot_prices['ETH'], rates['ETH'])

                hedge_trade_list = hedge_trade_list.append(pd.DataFrame({'Instrument': undl+'-PERPETUAL', 'Size': hedge_size}, index=['Trade']))
        #print('Position: {}, Hedge: {}'.format(position, hedge_trade_list[-1]))
    return need_to_process, hedge_trade_list, positions
    #Go through the trades to see sum up the individual positions
    
    
def delta_hedge(surface, size, strike, expiry, spot, rate):
    t = (expiry - pd.Timestamp.now().normalize()).days / 365
    iv = float(surface(t, strike))
    bs = BlackScholes(spot, strike, rate, 0, iv, t, 'call')
    return -bs.delta() * spot * size

def nearest_expiry(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))

def nearest_strikes(items, pivot):
    return sorted(items, key=lambda x: abs(x - pivot))
    
def instrument_lookup(call_options, strike, expiry):
    return call_options[(call_options['strike'] == strike) & (pd.DatetimeIndex(call_options['expiration']).tz_localize(None) == expiry)]['instrumentName'][0]

# choose closest expiry, select strike based on aggressive parameter: -10 to +10. -10 Select lower strike, 10 select upper strike
def hedge_trade(client, call_options, position, aggressive=-10):
    expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
    strike = position['strikePrice']
    size = position['amount']
    hedge_expiry_list = pd.DatetimeIndex(call_options['expiration']).tz_localize(None).unique()
    hedge_expiry = nearest_expiry(hedge_expiry_list, expiry)
    call_options['expiration'] = pd.DatetimeIndex(call_options['expiration']).tz_localize(None)
    hedge_option_list = call_options[call_options['expiration'] == hedge_expiry]
    hedge_strike_list = list(hedge_option_list['strike'])
    hedge_strikes = nearest_strikes(hedge_strike_list, strike)
    if len(hedge_strikes) > 2:
        strike1 = hedge_strikes[0]
        strike2 = hedge_strikes[1]
        strike1_dist = abs(strike1 - strike)
        strike2_dist = abs(strike2 - strike)
        if strike1 == strike:
            #instrument = call_options[(call_options['strike'] == strike1) & (pd.Timestamp(call_options['expiration']).tz_localize(None) == hedge_expiry)]['instrumentName'][0]
            instrument = instrument_lookup(call_options, strike1, hedge_expiry)
            return pd.DataFrame({'Instrument': instrument, 'Size': -size}, index=['Trade'])
        elif strike1 > strike:
            lower_strike_wgt_ratio = strike2_dist / (strike1_dist + strike2_dist)
            upper_strike_wgt_ratio = 1 - lower_strike_wgt_ratio
            lower_strike = strike2
            upper_strike = strike1
        else:
            lower_strike_wgt_ratio = strike1_dist / (strike1_dist + strike2_dist)
            upper_strike_wgt_ratio = 1 - lower_strike_wgt_ratio
            lower_strike = strike1
            upper_strike = strike2          
        lower_instrument = instrument_lookup(call_options, lower_strike, hedge_expiry)
        upper_instrument = instrument_lookup(call_options, upper_strike, hedge_expiry) 
        lower_size = norm.cdf(aggressive) * lower_strike_wgt_ratio * size
        upper_size = (size - lower_size)
        trade_list = pd.DataFrame()
        if upper_size > 0:
            trade_list = trade_list.append(pd.DataFrame({'Instrument': upper_instrument, 'Size': -upper_size}, index=['Trade']))
        if lower_size > 0:
            trade_list = trade_list.append(pd.DataFrame({'Instrument': lower_instrument, 'Size': -lower_size}, index=['Trade']))
        return trade_list
    elif len(hedge_strikes) == 1:
        instrument = instrument_lookup(call_options, hedge_strikes[0], hedge_expiry)
        return pd.DataFrame({'Instrument': instrument, 'Size': -size}, index=['Trade'])
    else:
        # Delta hedge
        return None
                
        
    #
    
    
# Calculate the hedge of the overall delta position. Don't include the new positions
def delta_hedge_portfolio(new_trades, divibit_positions, deribit_positions, coin='BTC', delta_gamma_limit=None, gamma_multi=3, delta_abs_limit=None, min_trade_size=100):
    existing_trades = [x for x in divibit_positions.index if x not in new_trades]
    existing_divibit_positions = divibit_positions.loc[existing_trades]
    existing_divibit_positions = existing_divibit_positions[pd.DatetimeIndex(existing_divibit_positions['Expiry']).tz_localize(None) > pd.Timestamp.now().normalize()]
    hedge_portfolio = deribit_positions.append(existing_divibit_positions)
    delta_hedge = 0
    if len(hedge_portfolio) > 0:
        if (delta_gamma_limit is not None) and (abs(hedge_portfolio.sum()['Delta']) > gamma_multi * (abs(hedge_portfolio.sum()['Gamma']))):
    	    delta_hedge = -hedge_portfolio.sum()['Delta']
        if (delta_abs_limit is not None) and (abs(hedge_portfolio.sum()['Delta']) >= min_trade_size):
            delta_hedge = -hedge_portfolio.sum()['Delta']
        return pd.DataFrame({'Instrument': coin+'-PERPETUAL', 'Size': delta_hedge}, index=['Trade'])
    return pd.DataFrame()
    # Aggregate the overall orders and send to deribit for execution


def deribit_rounding(instrument):
    if instrument == 'BTC-PERPETUAL':
        rounding = 1
    elif instrument == 'ETH-PERPETUAL':
        rounding = 1
    elif 'BTC' in instrument:
        rounding = 0.1
    elif 'ETH' in instrument:
        rounding = 1
    return rounding

def deribit_multiplier(instrument):
    if instrument == 'BTC-PERPETUAL':
        return 10
    return 1

def clean_deribit_order(new_trades):
    clean_new_trades = {}
    for instrument in new_trades:
        rounding = deribit_rounding(instrument)
        new_size = round(new_trades[instrument] / rounding) * rounding
        if abs(new_size) > 0:
            clean_new_trades[instrument] = new_size
    return clean_new_trades
    
def deribit_trade_pricing(new_trades, surfaces, spot_prices, rates, call_options):
    pricing = {}
    for trade in new_trades:
        if trade == 'BTC-PERPETUAL':
            pricing[trade] = spot_prices['BTC']
        elif trade == 'ETH-PERPETUAL':
            pricing[trade] = spot_prices['ETH']
        else:
            if 'BTC' in trade:
                option_info = call_options['BTC'].loc[trade]
                surface = surfaces['BTC']
                spot = spot_prices['BTC']
                rate = rates['BTC']
            else:
                option_info = call_options['ETH'].loc[trade]
                surface = surfaces['ETH']
                rate = rates['ETH']
                
            strike = option_info['strike']
            expiry = pd.Timestamp(option_info['expiration']).tz_localize(None)
            t = (expiry - pd.Timestamp.now().normalize()).days / 365
            iv = float(surface(t, strike))
            bs = BlackScholes(spot, strike, rate, 0, iv, t, 'call')
            pricing[trade] = bs.call_price() / spot
    return pricing
            
def generate_trades(client, token, spot_btc, rate_btc, surface_btc, call_options_btc, futures_btc, spot_eth, rate_eth, surface_eth, call_options_eth, futures_eth, min_hedge=100):
    deribit_btc_pos = get_deribit_greeks(client, spot_btc, rate_btc, surface_btc, call_options_btc, futures_btc, coin='BTC')
    deribit_eth_pos = get_deribit_greeks(client, spot_eth, rate_eth, surface_eth, call_options_eth, futures_eth, coin='ETH')
    divibit_btc_pos = get_divibit_greeks(token, surface_btc, spot_btc, rate_btc, coin='BTC')
    divibit_eth_pos = get_divibit_greeks(token, surface_eth, spot_eth, rate_eth, coin='ETH')
    all_btc_pos = deribit_btc_pos.append(divibit_btc_pos)
    all_eth_pos = deribit_eth_pos.append(divibit_eth_pos)
    
    
    call_options = {}
    call_options['BTC'] = call_options_btc
    call_options['ETH'] = call_options_eth
    surfaces = {}
    surfaces['BTC'] = surface_btc
    surfaces['ETH'] = surface_eth
    spot_prices = {}
    spot_prices['BTC'] = spot_btc
    spot_prices['ETH'] = spot_eth
    rates = {}
    rates['BTC'] = rate_btc
    rates['ETH'] = rate_eth
    new_trade_ids, new_trades, positions = process_new_divibit_trades(token, client, call_options, surfaces, spot_prices, rates, aggressive=-10)
    btc_hedge = delta_hedge_portfolio(new_trade_ids, divibit_btc_pos, deribit_btc_pos, coin='BTC', delta_gamma_limit=True, gamma_multi=3, min_trade_size=min_hedge)
    eth_hedge = delta_hedge_portfolio(new_trade_ids, divibit_eth_pos, deribit_eth_pos, coin='ETH', delta_gamma_limit=True, gamma_multi=3, min_trade_size=min_hedge)
    new_trades = new_trades.append(btc_hedge)
    new_trades = new_trades.append(eth_hedge)
    new_trades = new_trades.groupby(['Instrument']).sum()['Size'].to_dict()
    new_trades = clean_deribit_order(new_trades)
    trade_prices = deribit_trade_pricing(new_trades, surfaces, spot_prices, rates, call_options)
    return new_trades, trade_prices, positions, all_btc_pos, all_eth_pos

#Go through the new trades to do. Check if there are any open trades.
def process_orders(client, trade_list, trade_prices, positions, aggressive=10):
    for trade in trade_list:
        size = trade_list[trade]
        open_order_size = get_open_orders(client, trade)
        new_size = size - open_order_size
        print('{} existing outstanding order Size: {}, New Order Size: {}'.format(trade, open_order_size, new_size))
        if abs(new_size) > 0:
            price = trade_prices[trade]
            send_order(client, trade, new_size, price, aggressive=aggressive)
    positions.to_pickle('processed.p')

    
def get_open_orders(client, instrument):
    open_orders = pd.DataFrame(client.getopenorders(instrument))
    if len(open_orders) > 0:
        open_orders['dir'] = open_orders['direction'].apply(lambda x: 1 if x == 'buy' else -1)
        open_orders['amount'] = open_orders['amount'] * open_orders['dir']
        return open_orders['amount'].sum()
    return 0
    
def send_order(client, instrument, size, price, aggressive = 5, tick=0.0005):
    """Aggressive is scale of 1-10. 10 meaning hit the bid"""
    orderbook = client.getorderbook(instrument)
    bids = pd.DataFrame(orderbook['bids'])
    asks = pd.DataFrame(orderbook['asks'])
    rounding = deribit_rounding(instrument)
    multiplier = deribit_multiplier(instrument)
    
    print('Instrument: {}, Rounding: {}, Multi: {}, Size: {}'.format(instrument, rounding, multiplier, size))
    if len(bids) == 0:
        print('No bids for {}, sending price {}'.format(instrument, price))
        size = round(size / rounding) * rounding
        client.sell(instrument, -size, price)
        return
        #Throw an error
    bb = bids['price'].max()
    order_price = bb
    if len(asks) > 0:
        bo = asks['price'].min()
        buffer = int(round(((10 - aggressive) / 10 * (bo - bb)) / tick)) * tick
        order_price = bb + buffer

    if instrument == 'BTC-PERPETUAL' or instrument == 'ETH-PERPETUAL':
        order_price = price
    size = round(size / multiplier / rounding) * rounding
    if size < 0:
        print('Instrument: {}, Size: {}, Price: {}'.format(instrument, -size, order_price))
        client.sell(instrument, -size, order_price)
    elif size > 0:
        client.buy(instrument, size, order_price)





def main():

    sample_expiry = {
        'username': 'admin@payton',
        'password': 'divibit-admin-123'
    }
    head = {'Content-type': 'application/json'}
    url = 'https://dev-admin-api.divibit.com/v1/login'
    r = requests.post(url, headers=head, data=json.dumps(sample_expiry))
    token = r.json()['token']


    while(True):
        try:
            sentry_sdk.init("https://2e7756d69f1a4c6d9815ebd1ce4dadae@sentry.io/1459753")
            client = RestClient('5RQV61aMc3sra', 'D2JRQ22JLSK3RBXUXCCG33ACZFKR2MKB', url='https://www.deribit.com')
            surface_btc, spot_btc, rate_btc, call_options_btc, futures_btc = get_vol_surface(client, vol_type='mid', coin='BTC')
            surface_eth, spot_eth, rate_eth, call_options_eth, futures_eth = get_vol_surface(client, vol_type='mid', coin='ETH')
            client = RestClient('Js9aG3pPLaQb', 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO', url='https://test.deribit.com')

            new_trades, trade_prices, positions, all_btc_pos, all_eth_pos = generate_trades(client, token, 
                spot_btc, rate_btc, surface_btc, call_options_btc, futures_btc, 
                spot_eth, rate_eth, surface_eth, call_options_eth, futures_eth, min_hedge=30)
            all_btc_pos.to_pickle('all_btc_greeks.p')
            all_eth_pos.to_pickle('all_eth_greeks.p')
            print('{} New Trades: {}'.format(pd.Timestamp.now(), new_trades))
            process_orders(client, new_trades, trade_prices, positions, aggressive=10)
            
            time.sleep(60) 
        except KeyboardInterrupt:
            print('Manual break by user')
            capture_message('Deribit Trading Engine Manual Shutdown')
            return
        except Exception as e:
            capture_message('Deribit Trading Engine Exception Caught. Attempting to Restart: \n {}'.format(e))
            capture_exception(e)
            time.sleep(EXCEPTION_DELAY)
            sample_expiry = {
                'username': 'admin@payton',
                'password': 'divibit-admin-123'
            }
            head = {'Content-type': 'application/json'}
            url = 'https://dev-admin-api.divibit.com/v1/login'
            r = requests.post(url, headers=head, data=json.dumps(sample_expiry))
            token = r.json()['token']

if __name__=='__main__':
    main()