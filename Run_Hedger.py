import pandas as pd
import numpy as np
import json 
import requests
from InfluxDBHelper import write_ts_data, read_ts_data, read_latest_ts_data, read_filtered_ts_data, create_db, drop_db, object_to_hex, hex_to_object
import time
from Exchanges import Bitmex, Deribit, Divibit
from Hedger import Hedger


DERIBIT_TEST_KEY = 'Js9aG3pPLaQb'
DERIBIT_TEST_SECRET = 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO'
DERIBIT_PROD_KEY = '5RQV61aMc3sra'
DERIBIT_PROD_SECRET = 'D2JRQ22JLSK3RBXUXCCG33ACZFKR2MKB'
DIVIBIT_KEY = 'admin@payton'
DIVIBIT_SECRET = 'divibit-admin-123'
BITMEX_KEY = 'Zu5uSnYS-gikTF3HjPRaAGaB'
BITMEX_SECRET = 'Y5inOaP6s2PrUdEy4wbBQWgXMeCnfZHWyceueGstUjOinyRV'

LAST_DB_ENTRY_THRES = 60 * 30 #SECONDS SINCE THE LAST DATABASE ENTRY
RISK_REFRESH_TIME = 60 * 2 #SECONDS TO REFRESH THE HEDGING FREQUENCY

TRADE_AGGRESSIVE_PARAM = 10
BITMEX_SPREAD_PARAM = 0.25


def get_pricing_data(deribit):
	bid_iv_surface = {}
	mid_iv_surface = {}
	spot_price = {}
	rate = {}
	call_options = {}
	futures = {}
	for coin in ['BTC', 'ETH']:
		last_timestamp = read_latest_ts_data('pricing', 'Coin', coin, last_timestamp=True)
		if (pd.Timestamp.utcnow() - last_timestamp).seconds < LAST_DB_ENTRY_THRES:
			print('Getting pricing data from DB')
			last_db_entry = read_latest_ts_data('pricing', 'Coin', coin).iloc[0]
			bid_iv_surface[coin] = hex_to_object(last_db_entry['bid_iv_surface'])
			mid_iv_surface[coin] = hex_to_object(last_db_entry['mid_iv_surface'])
			_, _, spot_price[coin] = deribit.get_swap_price(coin)
			rate[coin] = last_db_entry['rate']
			call_options[coin] = hex_to_object(last_db_entry['call_options'])
			futures[coin] = hex_to_object(last_db_entry['futures'])
		else:
			print('Regenerating pricing data directly from exchange')
			bid_iv_surface[coin], mid_iv_surface[coin], spot_price[coin], rate[coin], call_options[coin], futures[coin] = deribit.get_call_options(coin=coin)
	return bid_iv_surface, mid_iv_surface, spot_price, rate, call_options, futures


def write_risk_to_influx(positions, coin):
	params = {}
	params['positions'] = object_to_hex(positions)
	params['total_delta'] = 0.0
	params['total_gamma'] = 0.0
	if len(positions) > 0:
		if 'Delta' in positions.columns:
			params['total_delta'] = positions['Delta'].sum()		
		if 'Gamma' in positions.columns:
			params['total_gamma'] = positions['Gamma'].sum()
	write_ts_data('positions', {'Exchange': 'Deribit', 'Coin': coin}, params)


def main():
	deribit_prod = Deribit(DERIBIT_PROD_KEY, DERIBIT_PROD_SECRET, test=False)
	deribit_test = Deribit(DERIBIT_TEST_KEY, DERIBIT_TEST_SECRET, test=True)
	divibit = Divibit(DIVIBIT_KEY, DIVIBIT_SECRET)
	bitmex = Bitmex(BITMEX_KEY, BITMEX_SECRET, test=True)

	while True:
		try:
			bid_iv_surface, mid_iv_surface, spot_price, rate, call_options, futures = get_pricing_data(deribit_prod)
			hedger = Hedger()
			new_trade_ids, new_trades, trade_prices, positions, all_btc_pos, all_eth_pos = hedger.generate_trades(divibit, deribit_test, bitmex, vol_surface=mid_iv_surface, irate=rate, call_options=call_options, futures=futures)
			write_risk_to_influx(all_btc_pos, 'BTC')
			write_risk_to_influx(all_eth_pos, 'ETH')
			hedger.process_orders(deribit_test, bitmex, new_trade_ids, new_trades, trade_prices, positions, aggressive=TRADE_AGGRESSIVE_PARAM, bitmex_spread=BITMEX_SPREAD_PARAM)
			time.sleep(RISK_REFRESH_TIME)
		except KeyboardInterrupt:
			print('Manual break by user')
			return
		except Exception as e:
			print('Exception caught: {}'.format(e))
			print('Pausing...')
			time.sleep(RISK_REFRESH_TIME)
			print('Reconnecting')
			deribit_prod = Deribit(DERIBIT_PROD_KEY, DERIBIT_PROD_SECRET, test=False)
			deribit_test = Deribit(DERIBIT_TEST_KEY, DERIBIT_TEST_SECRET, test=True)
			divibit = Divibit(DIVIBIT_KEY, DIVIBIT_SECRET)
			bitmex = Bitmex(BITMEX_KEY, BITMEX_SECRET, test=True)

if __name__== "__main__":
  main()