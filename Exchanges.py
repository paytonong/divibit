import pandas as pd
import numpy as np
from deribit_api import RestClient
import bitmex
from scipy.interpolate import interp2d, SmoothBivariateSpline, LinearNDInterpolator, NearestNDInterpolator
from OptionPricer import get_iv, BlackScholes
import requests
import json 

DIVIBIT_URL = 'https://dev-admin-api.divibit.com'

class LinearNDInterpolatorExt(object):
	def __init__(self, points, values):
		self.funcinterp = LinearNDInterpolator(points,values)
		self.funcnearest = NearestNDInterpolator(points,values)
	
	def __call__(self,*args):
		t = self.funcinterp(*args)
		if not np.isnan(t):
			return t.item(0)
		else:
			return self.funcnearest(*args)


def timestamp_to_epoch(timestamp):
	return int((timestamp - pd.Timestamp('1970-01-01')).total_seconds())


class DivibitLoginException(Exception):
   """Raised when the input value is too small"""
   print('Divibit Login Error')
   pass

class Exchange:
	def __init__(self, key, secret, test=True):
		self.key = key
		self.secret = secret
		self.client = self.init_client(key, secret, test=test)
		self.instruments = None

	def init_client(self, key, secret, test=True):
		pass

	def get_client(self):
		return client

	def get_swap_price(self, undl='BTC'):
		pass

	def get_total_wallet(self):
		pass

	def get_margin_utilized(self):
		pass

	def get_all_positions(self):
		pass


class Divibit(Exchange):
	def __init__(self, key, secret, test=True):
		super().__init__(key, secret, test=test)
		

	def init_client(self, key, secret, test=True):
		request_input = {'username': key, 'password': secret}
		head = {'Content-type': 'application/json'}
		url = DIVIBIT_URL + '/v1/login'
		r = requests.post(url, headers=head, data=json.dumps(request_input))
		if type(r.json()) == str:
			print('Error: Divibit not returning json request')
			print('Request return: {}'.format(r.json()))
			raise DivibitLoginException
		client = r.json()['token']
		return client

	def post_expiry(self, expiry, epoch=True, coin='BTC'):
		if epoch:
			epoch_expiry = [timestamp_to_epoch(x) for x in expiry]
			sample_expiry = {
				"expiry": epoch_expiry
			}
		else:
			sample_expiry = {
				"expiry": expiry
			}
		head = {'x-access-token': self.client, 'Content-type': 'application/json'}
		url = DIVIBIT_URL + '/v1/config/expiry-terms/' + coin
		r = requests.post(url, headers=head, data=json.dumps(sample_expiry))

	def get_expiry(self, coin='BTC'):
		url = DIVIBIT_URL + '/v1/config/expiry-terms/' + coin
		head = {'x-access-token': self.client}
		res = requests.get(url, headers=head)
		return res.text

	def post_strike(self, strikes, coin='BTC'):
		sample_strikes = {
			"strike": strikes
		}
		head = {'x-access-token': self.client, 'Content-type': 'application/json'}
		url = DIVIBIT_URL + '/v1/config/strike-prices/' + coin
		r = requests.post(url, headers=head, data=json.dumps(sample_strikes))

	def get_strike(self, coin='BTC'):
		url = DIVIBIT_URL + '/v1/config/strike-prices/' + coin
		head = {'x-access-token': self.client}
		res = requests.get(url, headers=head)
		return res.text

	def post_rate(self, ir, coin='BTC'):
		sample_rate = {
			"rate": ir
		}
		head = {'x-access-token': self.client, 'Content-type': 'application/json'}
		url = DIVIBIT_URL + '/v1/config/rate/' + coin
		r = requests.post(url, headers=head, data=json.dumps(sample_rate))

	def get_rate(self, coin='BTC'):
		url = DIVIBIT_URL + '/v1/config/rate/' + coin
		head = {'x-access-token': self.client}
		res = requests.get(url, headers=head)
		return res.text

	def post_vols(self, vols, coin='BTC'):
		sample_vols = {
			"volatility": vols
		}
		head = {'x-access-token': self.client, 'Content-type': 'application/json'}
		url = DIVIBIT_URL + '/v1/config/volatility/' + coin
		r = requests.post(url, headers=head, data=json.dumps(sample_vols))

	def post_all(self, all_dict, coin='BTC'):
		head = {'x-access-token': self.client, 'Content-type': 'application/json'}
		url = DIVIBIT_URL + '/v1/config/all/' + coin
		r = requests.post(url, headers=head, data=json.dumps(all_dict))

	def get_volatility(self, coin='BTC'):
		url = DIVIBIT_URL + '/v1/config/volatility/' + coin
		head = {'x-access-token': self.client}
		res = requests.get(url, headers=head)
		return res.text

	def get_trades(self):
		url = DIVIBIT_URL + '/v1/trades'
		head = {'x-access-token': self.client}
		res = requests.get(url, headers=head)
		positions = pd.DataFrame(res.json())
		positions.index = positions['id']
		positions['amount'] = positions['amount'].convert_objects(convert_numeric=True)
		positions['strikePrice'] = positions['strikePrice'].convert_objects(convert_numeric=True)
		time_diff = (pd.DatetimeIndex(positions['expiryTs']).tz_localize(None) - pd.Timestamp.utcnow().tz_localize(None))
		positions['t'] = (time_diff.days * 24 * 60 * 60 + time_diff.seconds) / (365 * 24 * 60 * 60)
		return positions

	def get_greeks(self, vol_surface, spot_price, irate, coin='BTC'):
		positions = self.get_trades()
		
		positions = positions[positions['assetId'] == coin]
		position_greeks = pd.DataFrame()
		for pos in positions.index:
			position = positions.loc[pos]
			if position['t'] > 0:
				iv = float(vol_surface(position['t'], position['strikePrice']))
				bs = BlackScholes(spot_price, position['strikePrice'], irate, 0, iv, position['t'], 'call')
				expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
				position_greeks = position_greeks.append(pd.DataFrame({'Delta': bs.delta() * spot_price * position['amount'],
																	   'Gamma': bs.gamma() / 100 * spot_price**2 * position['amount'],
																	   'Vega': bs.vega() / 100 * position['amount'],
																	   'Theta': bs.theta() * position['amount'],
																	   'Size': position['amount'],
																	   'Notional': spot_price * position['amount'],
																	   '% Strike': position['strikePrice'] / spot_price * 100,
																	   'Strike': position['strikePrice'],
																	   'Spot': spot_price,
																	   'Expiry': expiry,
																	   'Days to Expiry': (expiry - pd.Timestamp.now().normalize()).days,
																	   'IV': iv,
																	   'TradeID': position['id']}, index=[pos]))
		return position_greeks
		
class Deribit(Exchange):
	def __init__(self, key, secret, test=True):
		super().__init__(key, secret, test=test)
		self.futures = {}
		self.call_options = {}
		self.mid_iv_surface = {}
		self.bid_iv_surface = {}
		self.spot_price = {}
		self.rate = {}

	def init_client(self, key, secret, test=True):
		if test:
			client = RestClient(key, secret, url='https://test.deribit.com')
		else:
			client = RestClient(key, secret, url='https://deribit.com')
		return client


	def get_bid_ask_mid_price(self, instrument_name):
		'''
		Returns bid, ask, mid price of a given instrument
		'''
		orderbook = self.client.getorderbook(instrument_name)
		bids = pd.DataFrame(orderbook['bids'])
		asks = pd.DataFrame(orderbook['asks'])
		if len(bids) == 0 or len(asks) == 0:
			return np.nan, np.nan, np.nan
		return bids.iloc[0]['price'], asks.iloc[0]['price'], (bids.iloc[0]['price'] + asks.iloc[0]['price']) / 2


	def get_single_rate(self, futures, coin):
		swap_price = futures.loc[coin+'-PERPETUAL']['mid']
		rates = []
		for future in futures.index:
			if future != coin + '-PERPETUAL':
				current_future = futures.loc[future]
				time_diff = (pd.Timestamp(current_future['expiration']).tz_localize(None) - pd.Timestamp.utcnow().tz_localize(None))
				expiry_t = (time_diff.days * 24 * 60 * 60 + time_diff.seconds) / (365 * 24 * 60 * 60)
				current_future_rate = ((current_future['mid'] / swap_price) - 1) / expiry_t
				rates.append(current_future_rate)
		return np.mean(rates)
	
	def get_swap_price(self, coin='BTC'):
		return self.get_bid_ask_mid_price(coin + '-PERPETUAL')

	def get_d1_pricing(self, coin='BTC'):
		self.instruments = pd.DataFrame(self.client.getinstruments())
		self.futures[coin] = self.instruments[(self.instruments['kind'] == 'future') & (self.instruments['baseCurrency'] == coin)]
		self.futures[coin].index = self.futures[coin]['instrumentName']

		future_prices = {}
		for future in self.futures[coin].index:
			bid, ask, mid = self.get_bid_ask_mid_price(future)
			future_prices[future] = mid

		self.futures[coin]['mid'] = pd.Series(future_prices)
		spot_price = self.futures[coin].loc[coin + '-PERPETUAL']['mid']

		rate = self.get_single_rate(self.futures[coin], coin=coin)

		return spot_price, rate, self.futures[coin]


	def get_call_options(self, coin='BTC'):
		spot_price, rate, self.futures[coin] = self.get_d1_pricing(coin=coin)
		self.call_options[coin] = self.instruments[(self.instruments['kind'] == 'option') & (self.instruments['baseCurrency'] == coin) & (self.instruments['optionType'] == 'call')]
		self.call_options[coin].index = self.call_options[coin]['instrumentName']

		call_prices_mid = {}
		call_prices_bid = {}
		call_prices_ask = {}
		for call in self.call_options[coin].index:
			call_prices_bid[call], call_prices_ask[call], call_prices_mid[call] = self.get_bid_ask_mid_price(call)

		self.call_options[coin]['mid'] = pd.Series(call_prices_mid)
		self.call_options[coin]['bid'] = pd.Series(call_prices_bid)
		self.call_options[coin]['ask'] = pd.Series(call_prices_ask)

		time_diff = (pd.DatetimeIndex(self.call_options[coin]['expiration']).tz_localize(None) - pd.Timestamp.utcnow().tz_localize(None))
		self.call_options[coin]['t'] =  (time_diff.days * 24 * 60 * 60 + time_diff.seconds) / (365 * 24 * 60 * 60)
		self.call_options[coin] = self.insert_iv_to_df(self.call_options[coin], rate, spot_price)
		iv_options = self.call_options[coin][['strike', 't', 'IV bid']].dropna()
		cartcoord = list(zip(iv_options['t'].values, iv_options['strike'].values))
		bid_iv_surface = LinearNDInterpolatorExt(cartcoord, iv_options['IV bid'].values)

		iv_options = self.call_options[coin][['strike', 't', 'IV mid']].dropna()
		cartcoord = list(zip(iv_options['t'].values, iv_options['strike'].values))
		mid_iv_surface = LinearNDInterpolatorExt(cartcoord, iv_options['IV mid'].values)
		return bid_iv_surface, mid_iv_surface, spot_price, rate, self.call_options[coin], self.futures[coin]


	def insert_iv_to_df(self, call_options, rate, spot_price):
		today = pd.Timestamp.now().normalize()
		call_mid_iv = {}
		call_bid_iv = {}
		for call in call_options.index:
			call_option = call_options.loc[call]
			if ~np.isnan(call_option['mid']):
				call_mid_iv[call] = get_iv(call_option['mid'] * 100, 'call', 100, call_option['strike'] / spot_price * 100, rate, 0, call_option['t'])
			else:
				call_mid_iv[call] = np.nan

			if ~np.isnan(call_option['bid']):
				call_bid_iv[call] = get_iv(call_option['bid'] * 100, 'call', 100, call_option['strike'] / spot_price * 100, rate, 0, call_option['t'])
			else:
				call_bid_iv[call] = np.nan
		call_options['IV bid'] = pd.Series(call_bid_iv)
		call_options['IV mid'] = pd.Series(call_mid_iv)
		return call_options


	def instrument_rounding(self, instrument):
		if instrument == 'BTC-PERPETUAL':
			rounding = 1
		elif instrument == 'ETH-PERPETUAL':
			rounding = 1
		elif 'BTC' in instrument:
			rounding = 0.1
		elif 'ETH' in instrument:
			rounding = 1
		return rounding


	def instrument_multiplier(self, instrument):
		if instrument == 'BTC-PERPETUAL':
			return 10
		return 1


	def instrument_tick(self, instrument):
		if instrument == 'BTC-PERPETUAL':
			return 0.5
		elif instrument == 'ETH-PERPETUAL':
			return 0.05
		elif 'BTC' in instrument:
			return 0.0005
		elif 'ETH' in instrument:
			return 0.001


	def clean_order(self, new_trades):
		clean_new_trades = {}
		for instrument in new_trades:
			rounding = self.instrument_rounding(instrument)
			new_size = round(new_trades[instrument] / rounding) * rounding
			if abs(new_size) > 0:
				clean_new_trades[instrument] = new_size
		return clean_new_trades


	#Go through the new trades to do. Check if there are any open trades.
	def process_orders(self, trade_list, trade_prices, aggressive=10):
		for trade in trade_list:
			size = trade_list[trade]
			open_orders = self.get_open_orders(trade)
			if len(open_orders) > 0:
				for orderId in open_orders['orderId']:
					self.client.cancel(orderId)
			new_size = size
			print('{} existing outstanding order Size: {}, New Order Size: {}'.format(trade, open_order_size, new_size))
			if abs(new_size) > 0:
				price = trade_prices[trade]
				send_order(trade, new_size, price, aggressive=aggressive)
		# positions.to_pickle('processed.p')


	def send_order(self, instrument, size, price, aggressive=5, tick=0.0005):
		"""Aggressive is scale of 1-10. 10 meaning hit the bid"""
		orderbook = self.client.getorderbook(instrument)
		bids = pd.DataFrame(orderbook['bids'])
		asks = pd.DataFrame(orderbook['asks'])
		rounding = self.instrument_rounding(instrument)
		multiplier = self.instrument_multiplier(instrument)
		
		print('Instrument: {}, Rounding: {}, Multi: {}, Size: {}'.format(instrument, rounding, multiplier, size))
		if len(bids) == 0:
			print('No bids for {}, sending price {}'.format(instrument, price))
			size = round(size / rounding) * rounding
			self.client.sell(instrument, -size, price)
			return
			#Throw an error
		bb = bids['price'].max()
		order_price = bb
		if len(asks) > 0:
			bo = asks['price'].min()
			buffer = int(round(((10 - aggressive) / 10 * (bo - bb)) / tick)) * tick
			order_price = bb + buffer

		if instrument == 'BTC-PERPETUAL' or instrument == 'ETH-PERPETUAL':
			order_price = price
		size = round(size / multiplier / rounding) * rounding
		if size < 0:
			print('Instrument: {}, Size: {}, Price: {}'.format(instrument, -size, order_price))
			self.client.sell(instrument, -size, order_price)
		elif size > 0:
			self.client.buy(instrument, size, order_price)


	def get_open_orders(self, instrument):
		open_orders = pd.DataFrame(self.client.getopenorders(instrument))
		if len(open_orders) > 0:
			open_orders['dir'] = open_orders['direction'].apply(lambda x: 1 if x == 'buy' else -1)
			open_orders['amount'] = open_orders['amount'] * open_orders['dir']
			open_orders['orderID'] = open_orders['orderId']
			return open_orders[['instrument', 'orderID', 'amount', 'price']]
		return pd.DataFrame()


	def trade(self, instrument, size, order_price):
		multiplier = self.instrument_multiplier(instrument)
		rounding = self.instrument_rounding(instrument)
		size = round((size / multiplier) / rounding) * rounding

		if size < 0:
			self.client.sell(instrument, -size, order_price)
		else:
			self.client.buy(instrument, size, order_price)


	def amend_order(self, orderID, size, price):
		instrument = self.client.getopenorders(orderId=orderID)[0]['instrument']
		multiplier = self.instrument_multiplier(instrument)
		rounding = self.instrument_rounding(instrument)
		size = round(size / multiplier / rounding) * rounding

		self.client.edit(orderId=str(orderID), quantity=size, price=price)


	def cancel_order(self, orderID):
		self.client.cancel(orderId=str(orderID))		


	def get_total_wallet(self):
		deribit_details = self.client.account()
		return deribit_details['equity'] - deribit_details['maintenanceMargin']


	def get_margin_utilized(self):
		deribit_details = self.client.account()
		return deribit_details['maintenanceMargin']


	def get_all_positions(self, coin='BTC'):
		positions = pd.DataFrame(self.client.positions())
		if len(positions) == 0:
			return pd.DataFrame()
		positions.index = positions['instrument']
		positions = positions[positions['instrument'].str.contains(coin)]
		futures_overlap = [value for value in positions.index if value in self.futures[coin].index]
		if len(futures_overlap) > 0:
			positions = positions.combine_first(self.futures[coin].loc[positions.index])
		calls_overlap = [value for value in positions.index if value in self.call_options[coin].index]    
		if len(calls_overlap) > 0:
			positions = positions.combine_first(self.call_options[coin].loc[positions.index])
		#TODO check excpetion if the option is no longer traded (is this possible?)
		positions = positions[positions['instrument'].str.contains(coin)]
		_, _, spot_price = self.get_swap_price(coin)	
		positions['notional'] = spot_price * positions['amount']
		return positions


	def get_greeks(self, vol_surface=None, spot_price=None, irate=None, call_options=None, futures=None, coin='BTC'):
		if (vol_surface  is None) or (spot_price is None) or (irate is None) or (call_options is None) or (futures is None):
			self.bid_iv_surface[coin], self.mid_iv_surface[coin], self.spot_price[coin], self.rate[coin], self.call_options[coin], self.futures[coin] = self.get_call_options(coin=coin)
		else:
			self.mid_iv_surface[coin] = vol_surface
			self.spot_price[coin] = spot_price
			self.rate[coin] = irate
			self.call_options[coin] = call_options
			self.futures[coin] = futures

		positions = self.get_all_positions(coin=coin)
		position_greeks = pd.DataFrame()
		for pos in positions.index:
			position = positions.loc[pos]
			gamma = 0
			vega = 0
			theta = 0
			pct_strike = np.nan
			strike = np.nan
			iv = np.nan
			expiry = np.nan
			dte = np.nan
			if position['kind'] == 'option':
				expiry = pd.Timestamp(position['expiration']).tz_localize(None)
				dte = (expiry - pd.Timestamp.now().normalize()).days
				pct_strike = position['strike'] / self.spot_price[coin] * 100
				strike = position['strike']
				notional = self.spot_price[coin] * position['amount']
				if np.isnan(position['IV mid']):
					iv = float(self.mid_iv_surface[coin](position['t'], strike))
				else:
					iv = position['IV mid']
				bs = BlackScholes(self.spot_price[coin], position['strike'], self.rate[coin], 0, iv, position['t'], 'call')
				delta = bs.delta() * self.spot_price[coin] * position['amount']
				gamma = bs.gamma() / 100 * self.spot_price[coin]**2 * position['amount']
				vega = bs.vega() / 100 * position['amount']
				theta = bs.theta() * position['amount']

			else:
				delta = position['amount']
				notional = position['amount']
			
			position_greeks = position_greeks.append(pd.DataFrame({'Delta': delta,
																   'Gamma': gamma,
																   'Vega': vega,
																   'Theta': theta,
																   'Size': position['amount'],
																   'Notional': notional,
																   '% Strike': pct_strike,
																   'Strike': strike,
																   'Spot': self.spot_price[coin],
																   'Expiry': expiry,
																   'Days to Expiry': dte,
																   'IV': iv,
																   }, index=[pos]))
		return position_greeks




class Bitmex(Exchange):
	def __init__(self, key, secret, test=True):
		super().__init__(key, secret, test=test)
		self.coin_dict = {'BTC': 'XBTUSD',
							'ETH': 'ETHUSD'}
		self.spot_price = {}

	def init_client(self, key, secret, test=True):
		return bitmex.bitmex(test=test, api_key=key, api_secret=secret)


	def get_swap_price(self, coin='BTC'):
		symbol = self.coin_dict[coin]
			
		order_book = self.client.OrderBook.OrderBook_getL2(symbol=symbol, depth=1).result()[0]
		order_book = pd.DataFrame(order_book)
		bid = order_book[order_book['side'] == 'Buy']['price'].max()
		ask = order_book[order_book['side'] == 'Sell']['price'].min()
		mid = (bid + ask) / 2
		self.spot_price[coin] = mid
		return bid, ask, mid


	def get_bid_ask_mid_price(self, instrument_name):
		'''
		Returns bid, ask, mid price of a given instrument
		'''
		order_book = self.client.OrderBook.OrderBook_getL2(symbol=instrument_name, depth=1).result()[0]
		order_book = pd.DataFrame(order_book)
		bid = order_book[order_book['side'] == 'Buy']['price'].max()
		ask = order_book[order_book['side'] == 'Sell']['price'].min()
		mid = (bid + ask) / 2
		return bid, ask, mid

	def instrument_rounding(self, instrument):
		return 1


	def instrument_multiplier(self, instrument):
		return 1


	def instrument_tick(self, instrument):
		if instrument == 'XBTUSD':
			return 0.5
		elif instrument == 'ETHUSD':
			return 0.05

	def trade(self, instrument, size, order_price):
		multiplier = self.instrument_multiplier(instrument)
		rounding = self.instrument_rounding(instrument)
		size = round((size / multiplier) / rounding) * rounding
		self.client.Order.Order_new(symbol=instrument, orderQty=size, price=order_price).result()


	def get_total_wallet(self):
		results = self.client.User.User_getWalletSummary().result()
		full_wallet = pd.DataFrame(results[0])
		xbtusd = full_wallet[full_wallet['transactType'] == 'Total']
		btc = float(xbtusd['walletBalance'] + xbtusd['unrealisedPnl']) / 1e8
		return btc


	def get_all_positions(self, coin='BTC'):
		position = {}
		results = self.client.Position.Position_get().result()
		pos = results[0]
		if len(pos) > 0:
			total_position = pd.DataFrame(pos)
		else:
			total_position = None

		if total_position is not None and ~total_position.empty:
			if len(total_position[total_position['symbol'] == self.coin_dict[coin]]) > 0:
				position['amount'] = total_position[total_position['symbol'] == self.coin_dict[coin]]['currentQty'].sum()
			else:
				position['amount'] = 0
		else:
			position['amount'] = 0

		position = pd.DataFrame(position, index=[self.coin_dict[coin]])
		return position


	def get_open_orders(self, instrument):
		open_orders = pd.DataFrame(self.client.Order.Order_getOrders(filter=json.dumps({"open": True})).result()[0])
		if len(open_orders) > 0:
			open_orders = open_orders[open_orders['symbol'] == instrument]
		if len(open_orders) > 0:
			dir_map = {'Buy': 1, 'Sell': -1}
			open_orders['instrument'] = open_orders['symbol']
			open_orders['amount'] = open_orders['side'].map(dir_map) * open_orders['leavesQty']
			return open_orders[['instrument', 'orderID', 'amount', 'price']]
		return pd.DataFrame()


	def amend_order(self, orderID, size, price):
		self.client.Order.Order_amend(orderID=order, leavesQty=size, price=price).result()


	def cancel_order(self, orderID):
		self.client.Order.Order_cancel(orderID=orderID).result()


	def get_margin_utilized(self):
		margin = self.client.User.User_getMargin().result()[0]
		return margin['maintMargin'] / 1e8


	def get_liquidation_levels(self, coin='BTC'):
		pos = self.client.Position.Position_get().result()[0]
		
		if len(pos) > 0:
			total_position = pd.DataFrame(pos)
		else:
			total_position = None

		if total_position is not None and ~total_position.empty:
			if len(total_position[total_position['symbol'] == self.coin_dict[coin]]) > 0:
				liq_level = total_position[total_position['symbol'] == self.coin_dict[coin]]['liquidationPrice'].mean()
				_, _, spot_price = self.get_swap_price(coin=coin)
				return liq_level, (liq_level - spot_price) / spot_price * 100
		return 0

	def get_greeks(self, coin='BTC'):
		positions = self.get_all_positions(coin=coin)
		position_greeks = pd.DataFrame()
		for pos in positions.index:
			position = positions.loc[pos]
			notional = position['amount']
			position_greeks = position_greeks.append(pd.DataFrame({'Delta': notional,
																	'Notional': notional,
																	'Spot': self.spot_price[coin]}, index=[pos]))
		return position_greeks