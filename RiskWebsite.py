from flask import Flask
import pandas as pd
import sentry_sdk

app = Flask(__name__)

@app.route("/")
def hello():
    all_btc_pos = pd.read_pickle('all_btc_greeks.p')
    all_eth_pos = pd.read_pickle('all_eth_greeks.p')
    
    all_btc_pos = all_btc_pos.append(pd.DataFrame(all_btc_pos.sum().to_dict(), index=['Total']))
    all_eth_pos = all_eth_pos.append(pd.DataFrame(all_eth_pos.sum().to_dict(), index=['Total']))
    message_HTML = '<H3>All BTC Risk</H3>'
    message_HTML += all_btc_pos[['Delta', 'Gamma', 'Theta', 'Vega', 'Expiry', 'Strike', 'TradeID', 'Size', 'Notional', 'IV', 'Spot']].to_html()
    message_HTML += '<BR>'
    message_HTML += '<H3>All ETH Risk</H3>'
    message_HTML += all_eth_pos[['Delta', 'Gamma', 'Theta', 'Vega', 'Expiry', 'Strike', 'TradeID', 'Size', 'Notional', 'IV', 'Spot']].to_html()
    return message_HTML

if __name__ == "__main__":
    sentry_sdk.init("https://2e7756d69f1a4c6d9815ebd1ce4dadae@sentry.io/1459753")

    app.run()