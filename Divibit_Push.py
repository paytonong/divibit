from Exchanges import Bitmex, Deribit, Divibit
import pandas as pd
from datetime import timedelta 
from InfluxDBHelper import write_ts_data, read_ts_data, create_db, drop_db, object_to_hex
import time
# import ccxt

DERIBIT_TEST_KEY = 'Js9aG3pPLaQb'
DERIBIT_TEST_SECRET = 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO'
DERIBIT_PROD_KEY = '5RQV61aMc3sra'
DERIBIT_PROD_SECRET = 'D2JRQ22JLSK3RBXUXCCG33ACZFKR2MKB'
DIVIBIT_KEY = 'admin@payton'
DIVIBIT_SECRET = 'divibit-admin-123'

strike_list = [0.1, 0.15, 0.2, 0.3]
VOL_BUMP = -0.1
MIN_EXPIRY_DAYS = 3
MAX_EXPIRY_DAYS = 66
ADD_THIRD_WEEK = True
UPLOAD_REFRESH_RATE = 15 * 60 # Number of seconds per refresh to push data to database

def gen_vol_dict(vol_surface, spot_price, strikes, expiries, epoch=True, vol_bump=-0.1):
	vol_dict = {}
	for strike in strikes:
		vol_dict[str(strike)] = {}
		for expiry in expiries:
			if ~epoch and (isinstance(expiry, float) or isinstance(expiry, int)):
				vol_dict[str(strike)][str(expiry)] = float(vol_surface(expiry / 52, spot_price * (1 + strike))) / 100 + vol_bump
			elif isinstance(expiry, pd.Timestamp):
				epoch_expiry = timestamp_to_epoch(expiry)
				expiry_days = (expiry - pd.Timestamp.now().normalize()).days
				vol_dict[str(strike)][str(epoch_expiry)] = float(vol_surface(expiry_days / 365, spot_price * (1 + strike))) / 100 + vol_bump
	return vol_dict


def gen_all_dict(vol_surface, spot_price, strikes, expiries, rate, epoch=True, vol_bump=-0.1):
	vol_dict = {}
	for strike in strikes:
		vol_dict[str(strike)] = {}
		for expiry in expiries:
			if ~epoch and (isinstance(expiry, float) or isinstance(expiry, int)):
				vol_dict[str(strike)][str(expiry)] = float(vol_surface(expiry / 52, spot_price * (1 + strike))) / 100 + vol_bump
			elif isinstance(expiry, pd.Timestamp):
				epoch_expiry = timestamp_to_epoch(expiry)
				expiry_days = (expiry - pd.Timestamp.now().normalize()).days
				vol_dict[str(strike)][str(epoch_expiry)] = float(vol_surface(expiry_days / 365, spot_price * (1 + strike))) / 100 + vol_bump
	final_dict = {'rate': rate, 'volatility': vol_dict}
	return final_dict

def timestamp_to_epoch(timestamp):
	return int((timestamp - pd.Timestamp('1970-01-01')).total_seconds())

def gen_expiries(call_options, min_expiry_days=3, max_expiry_days=95, add_third_week=True):
	expiries = pd.DatetimeIndex(call_options['expiration']).tz_localize(None).unique()
	expiry_list = [x for x in expiries if (((x - pd.Timestamp.now().normalize()).days >= min_expiry_days) and ((x - pd.Timestamp.now().normalize()).days <= max_expiry_days))]
	expiry_list = sorted(expiry_list)
	if add_third_week:
		last_short_deribit_expiry = expiry_list[0]
		if (expiry_list[0] - pd.Timestamp.now()).days < 7:
			last_short_deribit_expiry = expiry_list[1]
		third_week = last_short_deribit_expiry + timedelta(days=7)
		expiry_list.append(third_week)
	return sorted(expiry_list)


def get_pricing_data(deribit):
	bid_iv_surface = {}
	mid_iv_surface = {}
	spot_price = {}
	rate = {}
	call_options = {}
	futures = {}
	for coin in ['BTC', 'ETH']:
		bid_iv_surface[coin], mid_iv_surface[coin], spot_price[coin], rate[coin], call_options[coin], futures[coin] = deribit.get_call_options(coin=coin)
		print('{}: Bid IV: {}, Mid IV: {}'.format(coin, bid_iv_surface[coin], mid_iv_surface[coin]))
		write_to_influx(bid_iv_surface[coin], mid_iv_surface[coin], spot_price[coin], rate[coin], call_options[coin], futures[coin], coin)
	return bid_iv_surface, mid_iv_surface, spot_price, rate, call_options, futures


def write_to_influx(bid_iv_surface, mid_iv_surface, spot_price, rate, call_options, futures, coin):
	params = {}
	params['bid_iv_surface'] = object_to_hex(bid_iv_surface)
	params['mid_iv_surface'] = object_to_hex(mid_iv_surface)
	params['1m_atm_vol'] = mid_iv_surface(1/12, spot_price)
	params['spot_price'] = spot_price
	params['rate'] = rate
	params['call_options'] = object_to_hex(call_options)
	params['futures'] = object_to_hex(futures)
	write_ts_data('pricing', {'Exchange': 'Deribit', 'Coin': coin}, params)


def post_to_divibit(divibit, bid_iv_surface, spot_price, rate, call_options, futures, vol_bump=-0.1, min_expiry_days=3, max_expiry_days=95, add_third_week=True):
	for coin in ['BTC', 'ETH']:
		expiry_list = gen_expiries(call_options[coin], min_expiry_days=min_expiry_days, max_expiry_days=max_expiry_days, add_third_week=add_third_week)
		all_json = gen_all_dict(bid_iv_surface[coin], spot_price[coin], strike_list, expiry_list, rate[coin], epoch=True, vol_bump=vol_bump)
		divibit.post_all(all_json, coin=coin)
		print('Posting all for {}'.format(coin))


def main():
	deribit_prod = Deribit(DERIBIT_PROD_KEY, DERIBIT_PROD_SECRET, test=False)
	divibit = Divibit(DIVIBIT_KEY, DIVIBIT_SECRET)

	while True:
		try:
			bid_iv_surface, mid_iv_surface, spot_price, rate, call_options, futures = get_pricing_data(deribit_prod)
			post_to_divibit(divibit, bid_iv_surface, spot_price, rate, call_options, futures, vol_bump=VOL_BUMP, min_expiry_days=MIN_EXPIRY_DAYS, max_expiry_days=MAX_EXPIRY_DAYS, add_third_week=ADD_THIRD_WEEK)
			time.sleep(UPLOAD_REFRESH_RATE)
		except KeyboardInterrupt:
			print('Manual break by user')
			return
		except Exception as e:
			print('Exception caught: {}'.format(e))
			print('Pausing...')
			time.sleep(UPLOAD_REFRESH_RATE)
			print('Reconnecting')
			deribit_prod = Deribit(DERIBIT_PROD_KEY, DERIBIT_PROD_SECRET, test=False)
			divibit = Divibit(DIVIBIT_KEY, DIVIBIT_SECRET)


if __name__== "__main__":
  main()