import pandas as pd
from datetime import timedelta 
# import ccxt
from influxdb import InfluxDBClient
import pickle

host = 'localhost'
port = 8086
username = 'root'
password = 'root'
database = 'demo'
influxdb = InfluxDBClient(host=host,
													port=port,
													username=username,
													password=password,
													database=database)

def create_db(database):
	influxdb.create_database(database)


def drop_db(database):
	influxdb.drop_database(database)


def object_to_hex(object):
	value = pickle.dumps(object)
	return value.hex()


def hex_to_object(hex):
	return pickle.loads(bytearray.fromhex(hex))

	
def write_ts_data(measurement: str, tags: dict, fields: dict,
									time: str = None):
		fields_keys_float = {}
		for key, value in fields.items():
				fields_keys_float[key] = value

		point = {
				"measurement": measurement,
				"tags": tags,
				"fields": fields_keys_float
		}
		if time is not None:
				point['time'] = time
		influxdb.write_points([point])

		
def read_ts_data(measurement: str):
		query_smt = 'select * from "' + measurement + '"'
		results = influxdb.query(query_smt)
		results_df = pd.DataFrame(
				list(results.get_points(measurement=measurement)))
		return results_df


def read_latest_ts_data(measurement: str, column_filter: str, column_filter_criteria: str, last_timestamp: bool = False):
		query_smt = 'select * from "' + measurement + '" where ' + column_filter + '=\'' + column_filter_criteria + '\' group by * order by desc limit 1;'
		results = influxdb.query(query_smt)
		results_df = pd.DataFrame(
				list(results.get_points(measurement=measurement)))
		if last_timestamp:
			return pd.Timestamp(results_df['time'].iloc[0])
		return results_df


def read_filtered_ts_data(measurement: str, column_filter: str, column_filter_criteria: str):
		query_smt = 'select * from "' + measurement + '" where ' + column_filter + '=\'' + column_filter_criteria + '\';'
		results = influxdb.query(query_smt)
		results_df = pd.DataFrame(
				list(results.get_points(measurement=measurement)))
		return results_df