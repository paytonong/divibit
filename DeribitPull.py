from deribit_api import RestClient
import pandas as pd
import numpy as np
import time
from scipy.interpolate import interp2d, SmoothBivariateSpline, LinearNDInterpolator, NearestNDInterpolator
from OptionPricer import get_iv, BlackScholes

class LinearNDInterpolatorExt(object):
  def __init__(self, points,values):
    self.funcinterp = LinearNDInterpolator(points,values)
    self.funcnearest = NearestNDInterpolator(points,values)
  def __call__(self,*args):
    t = self.funcinterp(*args)
    if not np.isnan(t):
      return t.item(0)
    else:
      return self.funcnearest(*args)

def get_mid_price(client, instrument_name, pricing_method='regular'):
    orderbook = client.getorderbook(instrument_name)
    bids = pd.DataFrame(orderbook['bids'])
    asks = pd.DataFrame(orderbook['asks'])
    if len(bids) == 0 or len(asks) == 0:
        return np.nan
    return (bids.iloc[0]['price'] + asks.iloc[0]['price']) / 2

def get_bid_price(client, instrument_name, pricing_method='regular'):
	orderbook = client.getorderbook(instrument_name)
	bids = pd.DataFrame(orderbook['bids'])
	if len(bids) == 0:
		return np.nan
	return bids.iloc[0]['price']

def get_single_rate(futures, coin):
    swap_price = futures.loc[coin+'-PERPETUAL']['mid']
    rates = []
    for future in futures.index:
        if future != coin + '-PERPETUAL':
            current_future = futures.loc[future]
            expiry_t = (pd.Timestamp(current_future['expiration']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365
            current_future_rate = ((current_future['mid'] / swap_price) - 1) / expiry_t
            rates.append(current_future_rate)
    return np.mean(rates)

def insert_iv_to_df(call_options, rate, spot_price, price_type='bid'):
    today = pd.Timestamp.now().normalize()
    call_iv = {}
    for call in call_options.index:
        call_option = call_options.loc[call]
        if (price_type is 'mid') and (~np.isnan(call_option['mid'])):
            call_iv[call] = get_iv(call_option['mid'] * 100, 'call', 100, call_option['strike'] / spot_price * 100, rate, 0, call_option['t'])
        elif (price_type is 'bid') and (~np.isnan(call_option['bid'])):
            call_iv[call] = get_iv(call_option['bid'] * 100, 'call', 100, call_option['strike'] / spot_price * 100, rate, 0, call_option['t'])
        else:
            call_iv[call] = np.nan
    call_options['IV'] = pd.Series(call_iv)
    return call_options

def get_vol_surface(client, vol_type='bid', coin='BTC'):
    instruments = pd.DataFrame(client.getinstruments())
    futures = instruments[(instruments['kind'] == 'future') & (instruments['baseCurrency'] == coin)]
    futures.index = futures['instrumentName']

    call_options = instruments[(instruments['kind'] == 'option') & (instruments['baseCurrency'] == coin) & (instruments['optionType'] == 'call')]
    call_options.index = call_options['instrumentName']

    call_prices_mid = {}
    call_prices_bid = {}
    for call in call_options.index:
        call_prices_mid[call] = get_mid_price(client, call)
        call_prices_bid[call] = get_bid_price(client, call)

    call_options['mid'] = pd.Series(call_prices_mid)
    call_options['bid'] = pd.Series(call_prices_bid)

    call_options['t'] = (pd.DatetimeIndex(call_options['expiration']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365

    future_prices = {}
    for future in futures.index:
        future_prices[future] = get_mid_price(client, future)

    futures['mid'] = pd.Series(future_prices)
    spot_price = futures.loc[coin + '-PERPETUAL']['mid']

    rate = get_single_rate(futures, coin=coin)

    call_options = insert_iv_to_df(call_options, rate, spot_price, price_type=vol_type)
    iv_options = call_options[['strike', 't', 'IV']].dropna()
    cartcoord = list(zip(iv_options['t'].values, iv_options['strike'].values))
    iv_surface = LinearNDInterpolatorExt(cartcoord, iv_options['IV'].values)
    return iv_surface, spot_price, rate, call_options, futures

def get_vols(client):
	spot_price = client.index()['btc']

	instruments = pd.DataFrame(client.getinstruments())
	futures = instruments[(instruments['kind'] == 'future') & (instruments['baseCurrency'] == 'BTC')]
	futures.index = futures['instrumentName']

	call_options = instruments[(instruments['kind'] == 'option') & (instruments['baseCurrency'] == 'BTC') & (instruments['optionType'] == 'call')]
	call_options.index = call_options['instrumentName']

	call_prices_mid = {}
	call_prices_bid = {}
	for call in call_options.index:
	    call_prices_mid[call] = get_mid_price(client, call)
	    call_prices_bid[call] = get_bid_price(client, call)

	call_options['mid'] = pd.Series(call_prices_mid)
	call_options['bid'] = pd.Series(call_prices_bid)

	call_options['t'] = (pd.DatetimeIndex(call_options['expiration']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365

	future_prices = {}
	for future in futures.index:
	    future_prices[future] = get_mid_price(client, future)

	futures['mid'] = pd.Series(future_prices)

	rate = get_single_rate(futures)

	call_options = insert_iv_to_df(call_options, rate, spot_price, price_type='bid')
	iv_options = call_options[['strike', 't', 'IV']].dropna()
	cartcoord = list(zip(iv_options['t'].values, iv_options['strike'].values))
	iv_surface = LinearNDInterpolator(cartcoord, iv_options['IV'].values)

	maturities = [1/12, 2/12, 3/12, 4/12, 6/12]
	strikes = [3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000]

	surface_df = {}
	for mat in maturities:
	    surface_df[mat] = {}
	    for strike in strikes:
	        surface_df[mat][strike] = iv_surface(mat, strike)
	pd.DataFrame(surface_df).to_csv('{}IV surface {}.csv'.format(SURFACE_PATH, pd.Timestamp.now()))
	print('Wrote surface at {}'.format(pd.Timestamp.now()))


def sell_option(call_options, expiry, strike, size, rounding=0.1):
    #sell closest strike call. 
    call_options['normalized expiry'] = pd.DatetimeIndex(call_options['expiration']).tz_localize(None).normalize()
    same_expiry_options = call_options[call_options['normalized expiry'] == expiry]
    if strike in list(same_expiry_options['strike']):
        return [same_expiry_options[same_expiry_options['strike'] == strike]['instrumentName'][0]], [size]
    
    upper_option = None
    lower_option = None
    if strike < same_expiry_options['strike'].max():
        upper_option = abs(same_expiry_options[same_expiry_options['strike'] > strike]['strike'] - strike).argmin()
    if strike > same_expiry_options['strike'].min():
        lower_option = abs(same_expiry_options[same_expiry_options['strike'] < strike]['strike'] - strike).argmin()    
    
    if upper_option is None:
        return [lower_option], [size]
    if lower_option is None:
        return [upper_option], [size]
    
    upper_option_dist = same_expiry_options.loc[upper_option]['strike'] - strike
    lower_option_dist = strike - same_expiry_options.loc[lower_option]['strike']
    lower_option_size = size * lower_option_dist / (upper_option_dist + lower_option_dist)
    lower_option_size = round(lower_option_size / rounding) * rounding
    upper_option_size = size - lower_option_size
    return [lower_option, upper_option], [lower_option_size, upper_option_size]

def send_order(client, instrument, size, aggressive = 5, tick=0.0005):
    """Aggressive is scale of 1-10. 10 meaning hit the bid"""
    orderbook = client.getorderbook(instrument)
    bids = pd.DataFrame(orderbook['bids'])
    asks = pd.DataFrame(orderbook['asks'])

    if len(bids) == 0:
        return
        #Throw an error
    bb = bids['price'].max()
    order_price = bb
    if len(asks) > 0:
        bo = asks['price'].min()
        buffer = int(round(((10 - aggressive) / 10 * (bo - bb)) / tick)) * tick
        order_price = bb + buffer
    print(order_price)
    client.sell(instrument, size, order_price)
    
    
def get_position_greeks(client, call_options):
    positions = pd.DataFrame(client.positions())
    positions.index = positions['instrument']
    positions = pd.concat([positions, call_options.loc[positions.index]], axis=1)
    positions['notional'] = positions['spot price'] * positions['amount']
    
    position_greeks = pd.DataFrame()
    for pos in positions.index:
        position = positions.loc[pos]
        bs = BlackScholes(position['spot price'], position['strike'], rate, 0, position['IV mid'], position['t'], 'call')
        position_greeks = position_greeks.append(pd.DataFrame({'Delta': bs.delta() * position['spot price'] * position['amount'],
                                                               'Gamma': bs.gamma() / 100 * position['spot price']**2 * position['amount'],
                                                               'Vega': bs.vega() / 100 * position['amount'],
                                                               'Theta': bs.theta() * position['amount']}, index=[pos]))
    position_greeks = position_greeks.append(pd.DataFrame(position_greeks.sum().to_dict(), index=['Total']))
    return position_greeks


# KEY = 'Js9aG3pPLaQb'
# SECRET = 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO'
# URL = 'https://test.deribit.com'


KEY = '5RQV61aMc3sra'
SECRET = 'D2JRQ22JLSK3RBXUXCCG33ACZFKR2MKB'
URL = 'https://www.deribit.com'

SURFACE_PATH = 'Deribit Surfaces/'

def main():
	client = RestClient(KEY, SECRET, url=URL)
	while True:
		try:
  			get_vols(client)
  			time.sleep(120)
		except KeyboardInterrupt:
			print('Manual break by user')
			return



if __name__== "__main__":
  main()

# client = RestClient('Js9aG3pPLaQb', 'OLNVLCV2QGQRN7XETZFGXN6LIK4IIBVO', url='https://test.deribit.com')
# spot_price = client.index()['btc']

# instruments = pd.DataFrame(client.getinstruments())
# futures = instruments[(instruments['kind'] == 'future') & (instruments['baseCurrency'] == 'BTC')]
# futures.index = futures['instrumentName']

# call_options = instruments[(instruments['kind'] == 'option') & (instruments['baseCurrency'] == 'BTC') & (instruments['optionType'] == 'call') & (instruments['strike'] > spot_price)]
# call_options.index = call_options['instrumentName']


# call_prices = {}
# for call in call_options.index:
#     call_prices[call] = get_mid_price(client, call)
    
# call_options['mid'] = pd.Series(call_prices)
# call_options['t'] = (pd.DatetimeIndex(call_options['expiration']).tz_localize(None) - pd.Timestamp.now().normalize()).days / 365

# future_prices = {}
# for future in futures.index:
#     future_prices[future] = get_mid_price(client, future)

# futures['mid'] = pd.Series(future_prices)

# rate = get_single_rate(futures)

# call_options = insert_iv_to_df(call_options, rate)
# iv_options = call_options[['strike', 't', 'IV']].dropna()
# cartcoord = list(zip(iv_options['t'].values, iv_options['strike'].values))
# iv_surface = LinearNDInterpolator(cartcoord, iv_options['IV'].values)