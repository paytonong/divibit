import pandas as pd
import numpy as np
from pathlib import Path
from OptionPricer import get_iv, BlackScholes
import pickle
from itertools import groupby
from scipy.stats import norm
from InfluxDBHelper import write_ts_data, read_ts_data, read_latest_ts_data, read_filtered_ts_data, create_db, drop_db, object_to_hex, hex_to_object

deribit_bitmex_map = {'BTC-PERPETUAL': 'XBTUSD', 'ETH-PERPETUAL': 'ETHUSD'}


def write_processed_trades_to_influx(processed_trades):
	params = {}
	params['processed'] = object_to_hex(processed_trades)
	write_ts_data('processed_trades', {'Exchange': 'Deribit'}, params)


class Hedger:
	def __init__(self):
		self.min_hedge = {'BTC': 0.1, 'ETH': 1}

	'''
	1. Get the list of processed divibit trades to generate new option trades
	2. Check the overall risk of the portfolio on how much to delta hedge, ignoring the new trades.
	3. Execute the trades
	'''

	def process_new_divibit_trades(self, divibit, deribit, aggressive=-10):
		positions = divibit.get_trades()
		# processed_file = Path('processed.p')
		# prev_processed_trades = None
		prev_processed_trades = hex_to_object(read_latest_ts_data('processed_trades', 'Exchange', 'Deribit').iloc[0]['processed'])
		# if processed_file.is_file():
		# 	prev_processed_trades = pickle.load( open( 'processed.p', 'rb' ) )
		if prev_processed_trades is not None:
			need_to_process = [tradeId for tradeId in positions.index if (tradeId not in prev_processed_trades.index) and (positions.loc[tradeId]['t'] > 0)]
		else:
			need_to_process = [tradeId for tradeId in positions.index if (positions.loc[tradeId]['t'] > 0)]
		print(need_to_process)
		hedge_trade_list = pd.DataFrame()
		for pos in need_to_process:
			position = positions.loc[pos]
			undl = position['assetId']
			hedge_order = None
			pos_call_option = deribit.call_options[undl]
			pos_spot_price = deribit.spot_price[undl]
			pos_surface = deribit.mid_iv_surface[undl]
			pos_rate = deribit.rate[undl]

			if position['amount'] >= self.min_hedge[undl]:
				hedge_order = self.hedge_trade(pos_call_option, position, aggressive=aggressive)
				if isinstance(hedge_order, pd.DataFrame):
					hedge_trade_list = hedge_trade_list.append(hedge_order)
			
			if (position['amount'] < self.min_hedge[undl]) or (hedge_order is None):
				expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
				hedge_size = self.delta_hedge(pos_surface, position['amount'], position['strikePrice'], expiry, pos_spot_price, pos_rate)
				#change this to a generic swap instrument to decide later which exchange to route the swap order to
				hedge_trade_list = hedge_trade_list.append(pd.DataFrame({'Instrument': undl+'-PERPETUAL', 'Size': hedge_size}, index=['Trade']))
		return need_to_process, hedge_trade_list, positions


	def delta_hedge(self, surface, size, strike, expiry, spot, rate):
		time_diff = expiry - pd.Timestamp.utcnow().tz_localize(None)
		t = (time_diff.days * 24 * 60 * 60 + time_diff.seconds) / (365 * 24 * 60 * 60)
		iv = float(surface(t, strike))
		bs = BlackScholes(spot, strike, rate, 0, iv, t, 'call')
		return -bs.delta() * spot * size


	def nearest_expiry(self, items, pivot):
		return min(items, key=lambda x: abs(x - pivot))


	def nearest_strikes(self, items, pivot):
		return sorted(items, key=lambda x: abs(x - pivot))


	def instrument_lookup(self, call_options, strike, expiry):
		return call_options[(call_options['strike'] == strike) & (pd.DatetimeIndex(call_options['expiration']).tz_localize(None) == expiry)]['instrumentName'][0]


	 # choose closest expiry, select strike based on aggressive parameter: -10 to +10. -10 Select lower strike, 10 select upper strike
	def hedge_trade(self, call_options, position, aggressive=-10):
		expiry = pd.Timestamp(position['expiryTs']).tz_localize(None)
		strike = position['strikePrice']
		size = position['amount']
		hedge_expiry_list = pd.DatetimeIndex(call_options['expiration']).tz_localize(None).unique()
		hedge_expiry = self.nearest_expiry(hedge_expiry_list, expiry)
		call_options['expiration'] = pd.DatetimeIndex(call_options['expiration']).tz_localize(None)
		hedge_option_list = call_options[call_options['expiration'] == hedge_expiry]
		hedge_strike_list = list(hedge_option_list['strike'])
		hedge_strikes = self.nearest_strikes(hedge_strike_list, strike)
		if len(hedge_strikes) > 2:
			strike1 = hedge_strikes[0]
			strike2 = hedge_strikes[1]
			strike1_dist = abs(strike1 - strike)
			strike2_dist = abs(strike2 - strike)
			if strike1 == strike:
				#instrument = call_options[(call_options['strike'] == strike1) & (pd.Timestamp(call_options['expiration']).tz_localize(None) == hedge_expiry)]['instrumentName'][0]
				instrument = self.instrument_lookup(call_options, strike1, hedge_expiry)
				return pd.DataFrame({'Instrument': instrument, 'Size': -size}, index=['Trade'])
			elif strike1 > strike:
				lower_strike_wgt_ratio = strike2_dist / (strike1_dist + strike2_dist)
				upper_strike_wgt_ratio = 1 - lower_strike_wgt_ratio
				lower_strike = strike2
				upper_strike = strike1
			else:
				lower_strike_wgt_ratio = strike1_dist / (strike1_dist + strike2_dist)
				upper_strike_wgt_ratio = 1 - lower_strike_wgt_ratio
				lower_strike = strike1
				upper_strike = strike2          
			lower_instrument = self.instrument_lookup(call_options, lower_strike, hedge_expiry)
			upper_instrument = self.instrument_lookup(call_options, upper_strike, hedge_expiry) 
			lower_size = norm.cdf(aggressive) * lower_strike_wgt_ratio * size
			upper_size = (size - lower_size)
			trade_list = pd.DataFrame()
			if upper_size > 0:
				trade_list = trade_list.append(pd.DataFrame({'Instrument': upper_instrument, 'Size': -upper_size}, index=['Trade']))
			if lower_size > 0:
				trade_list = trade_list.append(pd.DataFrame({'Instrument': lower_instrument, 'Size': -lower_size}, index=['Trade']))
			return trade_list
		elif len(hedge_strikes) == 1:
			instrument = self.instrument_lookup(call_options, hedge_strikes[0], hedge_expiry)
			return pd.DataFrame({'Instrument': instrument, 'Size': -size}, index=['Trade'])
		else:
			# Delta hedge
			return None


	def generate_trades(self, divibit, deribit, bitmex, vol_surface=None, irate=None, call_options=None, futures=None, min_hedge=100):
		all_btc_pos = pd.DataFrame()
		all_eth_pos = pd.DataFrame()
		spot_price = {}
		_, _, spot_price['BTC'] = deribit.get_swap_price('BTC')	
		_, _, spot_price['ETH'] = deribit.get_swap_price('ETH')
		bitmex.get_swap_price('BTC')
		bitmex.get_swap_price('ETH')	
		if (vol_surface  is None) or (spot_price is None) or (irate is None) or (call_options is None) or (futures is None):
			deribit_btc_pos = deribit.get_greeks(coin='BTC')
			deribit_eth_pos = deribit.get_greeks(coin='ETH')
		else:			
			deribit_btc_pos = deribit.get_greeks(vol_surface=vol_surface['BTC'], spot_price=spot_price['BTC'], irate=irate['BTC'], call_options=call_options['BTC'], futures=futures['BTC'], coin='BTC')
			deribit_eth_pos = deribit.get_greeks(vol_surface=vol_surface['ETH'], spot_price=spot_price['ETH'], irate=irate['ETH'], call_options=call_options['ETH'], futures=futures['ETH'], coin='ETH')
		divibit_btc_pos = divibit.get_greeks(vol_surface['BTC'], spot_price['BTC'], irate['BTC'], coin='BTC')
		divibit_eth_pos = divibit.get_greeks(vol_surface['ETH'], spot_price['ETH'], irate['ETH'], coin='ETH')
		bitmex_btc_pos = bitmex.get_greeks(coin='BTC')
		bitmex_eth_pos = bitmex.get_greeks(coin='ETH')

		all_btc_pos = pd.concat([divibit_btc_pos, deribit_btc_pos, bitmex_btc_pos])
		all_eth_pos = pd.concat([divibit_eth_pos, deribit_eth_pos, bitmex_eth_pos])
	
		new_trade_ids, new_trades, positions = self.process_new_divibit_trades(divibit, deribit, aggressive=-10)
		btc_hedge = self.delta_hedge_portfolio(new_trade_ids, divibit_btc_pos, deribit_btc_pos, bitmex_btc_pos, coin='BTC', delta_gamma_limit=True, gamma_multi=3, min_trade_size=min_hedge)
		eth_hedge = self.delta_hedge_portfolio(new_trade_ids, divibit_eth_pos, deribit_eth_pos, bitmex_eth_pos, coin='ETH', delta_gamma_limit=True, gamma_multi=3, min_trade_size=min_hedge)
		new_trades = new_trades.append(btc_hedge)
		new_trades = new_trades.append(eth_hedge)
		new_trades = new_trades.groupby(['Instrument']).sum()['Size'].to_dict()
		new_trades = self.clean_deribit_order(new_trades)
		trade_prices = self.deribit_trade_pricing(new_trades, vol_surface, spot_price, irate, call_options)
		return new_trade_ids, new_trades, trade_prices, positions, all_btc_pos, all_eth_pos


	# Calculate the hedge of the overall delta position. Don't include the new positions
	def delta_hedge_portfolio(self, new_trades, divibit_positions, deribit_positions, bitmex_positions, coin='BTC', delta_gamma_limit=None, gamma_multi=3, delta_abs_limit=None, min_trade_size=100):
		existing_trades = [x for x in divibit_positions.index if x not in new_trades]
		existing_divibit_positions = divibit_positions.loc[existing_trades]
		if len(existing_divibit_positions) > 0:
			existing_divibit_positions = existing_divibit_positions[pd.DatetimeIndex(existing_divibit_positions['Expiry']).tz_localize(None) > pd.Timestamp.utcnow().tz_localize(None)]
		hedge_portfolio = pd.concat([existing_divibit_positions, deribit_positions, bitmex_positions])
		delta_hedge = 0
		if len(hedge_portfolio) > 0:
			# if Gamma is not in the hedge portfolio columns, there are no options in the portfolio (likely no divibit positions)
			if 'Gamma' in hedge_portfolio.columns:
				if (delta_gamma_limit is not None) and (abs(hedge_portfolio.sum()['Delta']) > gamma_multi * (abs(hedge_portfolio.sum()['Gamma']))):
					delta_hedge = -hedge_portfolio.sum()['Delta']
			if (delta_abs_limit is not None) and (abs(hedge_portfolio.sum()['Delta']) >= min_trade_size):
				delta_hedge = -hedge_portfolio.sum()['Delta']
			return pd.DataFrame({'Instrument': coin+'-PERPETUAL', 'Size': delta_hedge}, index=['Trade']) #change this to the generic swap 
		return pd.DataFrame()


	def deribit_rounding(self, instrument):
		if instrument == 'BTC-PERPETUAL':
			rounding = 10
		elif instrument == 'ETH-PERPETUAL':
			rounding = 1
		elif 'BTC' in instrument:
			rounding = 0.1
		elif 'ETH' in instrument:
			rounding = 1
		return rounding


	def deribit_multiplier(self, instrument):
		# if instrument == 'BTC-PERPETUAL':
		# 	return 10
		return 1


	def clean_deribit_order(self, new_trades):
		clean_new_trades = {}
		for instrument in new_trades:
			rounding = self.deribit_rounding(instrument)
			new_size = round(new_trades[instrument] / rounding) * rounding
			if abs(new_size) > 0:
				clean_new_trades[instrument] = new_size
		return clean_new_trades
		

	def deribit_trade_pricing(self, new_trades, surfaces, spot_prices, rates, call_options):
		pricing = {}
		for trade in new_trades:
			if trade == 'BTC-PERPETUAL':
				pricing[trade] = spot_prices['BTC']
			elif trade == 'ETH-PERPETUAL':
				pricing[trade] = spot_prices['ETH']
			else:
				if 'BTC' in trade:
					option_info = call_options['BTC'].loc[trade]
					surface = surfaces['BTC']
					spot = spot_prices['BTC']
					rate = rates['BTC']
				else:
					option_info = call_options['ETH'].loc[trade]
					surface = surfaces['ETH']
					rate = rates['ETH']
					
				strike = option_info['strike']
				expiry = pd.Timestamp(option_info['expiration']).tz_localize(None)
				t = (expiry - pd.Timestamp.utcnow().tz_localize(None)).seconds / 365 * 24 * 60 * 60
				iv = float(surface(t, strike))
				bs = BlackScholes(spot, strike, rate, 0, iv, t, 'call')
				pricing[trade] = bs.call_price() / spot
		return pricing


	# Go through the new trades to do. Check if there are any open trades
	def process_orders(self, deribit, bitmex, new_trade_ids, trade_list, trade_prices, positions, aggressive=10, bitmex_spread=0.25):
		'''bitmex_spread = how much better in pricing in % the swap product is compared to deribit. 0.25 = 25bp '''
		for trade in trade_list:
			size = trade_list[trade]
			client = deribit
			send_bitmex = False
			if trade in deribit_bitmex_map:
				coin = trade.split('-')[0]
				deribit_bid, deribit_ask, deribit_mid = deribit.get_swap_price(coin)
				bitmex_bid, bitmex_ask, bitmex_mid = bitmex.get_swap_price(coin)
				bitmex_deribit_bid_spread = (bitmex_bid / deribit_bid - 1) * 100
				bitmex_deribit_ask_spread = -(bitmex_ask / deribit_ask - 1) * 100
				if (np.sign(size) > 0) and (bitmex_deribit_ask_spread > bitmex_spread):
					client = bitmex
					trade = deribit_bitmex_map[trade]
					send_bitmex = True
				elif (np.sign(size) < 0) and (bitmex_deribit_bid_spread > bitmex_spread):
					client = bitmex
					trade = deribit_bitmex_map[trade]
					send_bitmex = True
			open_order_size = self.get_open_orders(client, trade)
			new_size = size - open_order_size
			exchange_name = 'Deribit'
			if send_bitmex:
				exchange_name = 'Bitmex'
			print('{}: {} existing outstanding order Size: {}, New Order Size: {}'.format(exchange_name, trade, open_order_size, new_size))
			if abs(new_size) > 0:
				if send_bitmex:
					if new_size > 0:
						price = bitmex_ask
					elif new_size < 0:
						price = bitmex_bid
				else:
					price = trade_prices[trade]
				self.send_order(client, trade, new_size, price, aggressive=aggressive)
		if len(new_trade_ids) > 0:
			# positions.to_pickle('processed.p')
			write_processed_trades_to_influx(positions)
		
	def get_open_orders(self, client, instrument):
		open_orders = pd.DataFrame(client.get_open_orders(instrument))
		if len(open_orders) > 0:
			return open_orders['amount'].sum()
		return 0
		

	def send_order(self, client, instrument, size, price, aggressive=5):
		"""Aggressive is scale of 1-10. 10 meaning hit the bid"""
		bb, bo, mid = client.get_bid_ask_mid_price(instrument)
		rounding = client.instrument_rounding(instrument)
		multiplier = client.instrument_multiplier(instrument)
		
		print('Instrument: {}, Rounding: {}, Multi: {}, Size: {}'.format(instrument, rounding, multiplier, size))

		# order_size = round((size / multiplier) / rounding) * rounding
		order_size = size
		if (not np.isnan(bb)) and (not np.isnan(bo)):
			tick = client.instrument_tick(instrument)
			buffer = int(round(((10 - aggressive) / 10 * (bo - bb)) / tick)) * tick
			if order_size < 0:
				order_price = bb + buffer
			else:
				order_price = bo - buffer
		client.trade(instrument, order_size, order_price)