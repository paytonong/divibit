from scipy.stats import norm
import numpy as np

class BlackScholes:
    def __init__(self, s, k, r, q, vol, t, payoff):
        """vol is expressed in %. eg enter 16v as 16, not 0.16"""
        self.s = s
        self.k = k
        self.r = r
        self.q = q
        self.vol = vol / 100
        self.t = t
        self.payoff = payoff
        
    def d1(self):
        return (np.log(self.s / self.k) + 
                (self.r - self.q + self.vol ** 2 / 2) * \
                self.t) / (self.vol * np.sqrt(self.t))

    def d2(self):
        return (np.log(self.s / self.k) + 
                (self.r - self.q - self.vol ** 2 / 2) * \
                self.t) / (self.vol * np.sqrt(self.t))

    def phi(self, x):
        return np.exp(-x ** 2 / 2) / np.sqrt(2 * np.pi)

    def price(self):
        if self.payoff.lower() == 'put':
            return self.put_price()
        else:
            return self.call_price()
        
    def call_price(self):
        if self.t == 0:
            return 0
        return self.s * np.exp(-self.q * self.r) * norm.cdf(self.d1()) - np.exp(-self.r * self.t) * self.k * norm.cdf(self.d2())
    
    def put_price(self):
        if self.t == 0:
            return 0
        return np.exp(-self.r * self.t) * self.k * norm.cdf(-self.d2()) - self.s * np.exp(-self.q * self.r) * norm.cdf(-self.d1()) 
    
    def delta(self):
        if self.t == 0:
            return 0
        if self.payoff.lower() == 'put':
            return -np.exp(-self.q * self.r) * norm.cdf(-self.d1())
        else:
            return np.exp(-self.q * self.r) * norm.cdf(self.d1())
    
    def vega(self):
        if self.t == 0:
            return 0
        return self.s * np.exp(-self.q * self.r) * self.phi(self.d1()) * np.sqrt(self.t)
    
    def alt_vega(self):
        return self.k * np.exp(-self.r * self.t) * self.phi(self.d2()) * np.sqrt(self.t)
    
    def gamma(self):
        if self.t == 0:
            return 0
        return np.exp(-self.q * self.r) * self.phi(self.d1()) / (self.s * self.vol * np.sqrt(self.t))
    
    def theta(self):
        """Price of the option one calendar day later compared to the price today, rather than black scholes theta"""
        t_decay = self.t - 1 / 365
        tomorrow_option = BlackScholes(self.s, self.k, self.r, self.q, self.vol * 100, t_decay, self.payoff)
        return tomorrow_option.price() - self.price()
#         if self.payoff.lower() == 'put':
#             return -np.exp(-self.q * self.t) * (self.s * self.phi(self.d1()) * self.vol) / (2 * np.sqrt(self.t)) + \
#             self.r * self.k * np.exp(-self.r * self.t) * norm.cdf(-self.d2()) - \
#             self.q * self.s * np.exp(-self.q * self.r) * norm.cdf(-self.d1())
#         else:
#             return -np.exp(-self.q * self.t) * (self.s * self.phi(self.d1()) * self.vol) / (2 * np.sqrt(self.t)) - \
#             self.r * self.k * np.exp(-self.r * self.t) * norm.cdf(self.d2()) + \
#             self.q * self.s * np.exp(-self.q * self.r) * norm.cdf(self.d1())
            
    
    
    
def get_iv(mkt_price, payoff, s, k, r, q, t, start_guess=0.6):
    MAX_ITERATIONS = 100
    PRECISION = 1.0e-5
    
    vol = start_guess
    for i in range(0, MAX_ITERATIONS):
        option = BlackScholes(s, k, r, q, vol * 100, t, payoff)
        price = option.price()
        vega = option.vega()
        
        diff = mkt_price - price
        if abs(diff) < PRECISION:
            return vol * 100
        vol = vol + diff / vega
    return vol * 100